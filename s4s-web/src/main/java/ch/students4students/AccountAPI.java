package ch.students4students;

import ch.kalajdzic.services.rest.ContentTypes;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.students4students.data.Account;
import ch.students4students.data.AccountRole;
import ch.students4students.data.AccountRoles;
import ch.students4students.data.DiscordUsername;
import ch.students4students.data.EpflMail;
import ch.students4students.data.PersonalMail;
import ch.students4students.data.TelegramUsername;
import ch.students4students.data.ids.AccountId;
import ch.students4students.database.AccountsRepository;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/api/account")
public class AccountAPI {

    private final ObjectWriter basicWriter = Utils.objectMapper.writerFor(BasicData.class);
    private final AccountsRepository accounts;
    //    private final CandidatesRepository candidates;
//    private final CoursesRepository courses;
    private final LoginAPI loginAPI;

    public AccountAPI(AccountsRepository accounts, LoginAPI loginAPI) {
        this.accounts = accounts;
        this.loginAPI = loginAPI;
    }

    @Get
    @Path("/basic")
    @ContentType(ContentTypes.JSON)
    public ResponseConsumer basicData() {
        return loginAPI.chainLogin((e, accountId) -> {
            Account account = accounts.findById(accountId).orElseThrow();
            e.setResponseBody(basicWriter.writeValueAsBytes(new BasicData(account)));
        });
    }
//
//    @Get
//    @Path("/rc/courseName")
//    @ContentType(ContentTypes.PLAIN_TEXT)
//    public ResponseConsumer courseName() {
//        return loginAPI.chainLogin((e, accountId) -> {
//            Optional<Course> course = courses.getCourseForRC(accountId);
//            if (course.isPresent()) {
//                e.setResponseBody(course.get().id().value());
//            } else {
//                e.setError(HTTP_FORBIDDEN);
//            }
//        });
//    }

    //    @Get
//    @Path("/rc/candidates")
//    @ContentType(ContentTypes.JSON)
//    public ResponseConsumer candidates() {
//        return loginAPI.chainLogin((e, accountId) -> {
//            Optional<Course> course = courses.getCourseForRC(accountId);
//            if (course.isPresent()) {
//                List<Candidate> list = candidates.forCourse(course.get().id());
//                e.setResponseBody(candidatesWriter.writeValueAsBytes(list));
//            } else {
//                e.setError(HTTP_FORBIDDEN);
//            }
//        });
//    }
//
//    @Get
//    @Path("/rc/choices")
//    @ContentType(ContentTypes.JSON)
//    public ResponseConsumer getChoices() {
//        return loginAPI.chainLogin((e, accountId) -> {
//            Optional<Course> course = courses.getCourseForRC(accountId);
//            if (course.isPresent()) {
//                e.setResponseBody(courseWriter.writeValueAsBytes(course.get()));
//            } else {
//                e.setError(HTTP_FORBIDDEN);
//            }
//        });
//    }
//
//    @Post
//    @Path("/rc/choices")
//    @ContentType(ContentTypes.PLAIN_TEXT)
//    public ResponseConsumer setChoices() {
//        return loginAPI.chainLogin((e, accountId) -> {
//            Optional<Course> course = courses.getCourseForRC(accountId);
//            if (course.isPresent()) {
//                Course newCourse = courseReader.readValue(e.getRequestBody());
//                courses.update(course.get().id(), newCourse.selections());
//                e.setResponseBody("SUCCESS");
//            } else {
//                e.setError(HTTP_FORBIDDEN);
//            }
//        });
//    }
//
//
    public record BasicData(
            AccountId id,
            String displayName,
            String firstName,
            String lastName,
            PersonalMail personalMail,
            boolean verifiedPersonalMail,
            EpflMail epflMail,
            boolean verifiedEpflMail,
            Long telegramId,
            TelegramUsername telegramUsername,
            boolean verifiedTelegram,
            Long discordId,
            DiscordUsername discordUsername,
            boolean verifiedDiscord,
            Set<String> roles) {
        BasicData(Account account) {
            this(account.id(),
                    account.displayName(),
                    account.firstName(),
                    account.lastName(),
                    account.personalMail().orElse(null),
                    account.verifiedPersonalMail(),
                    account.epflMail().orElse(null),
                    account.verifiedEpflMail(),
                    account.telegramId().orElse(null),
                    account.telegramUsername().orElse(null),
                    account.verifiedTelegram(),
                    account.discordId().orElse(null),
                    account.discordUsername().orElse(null),
                    account.verifiedDiscord(),
                    account.roles().roles().stream().map(AccountRole::id).collect(Collectors.toSet()));
        }
    }
}
