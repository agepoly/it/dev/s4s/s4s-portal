package ch.students4students;

public record S4SConfig(
        String SERVER_PORT,
        String SERVER_MAX_CONNECTIONS,
        String DISCORD_APP_TOKEN,
        String TELEGRAM_BOT_TOKEN,
        String DATABASE_SERVER,
        String DATABASE_PORT,
        String DATABASE_NAME,
        String DATABASE_USER,
        String DATABASE_PASSWORD,
        String DATABASE_MAX_CONNECTION,
        String ADMIN_PASSWORD_HASH_HEX,
        String DATABASE_CACHE_SIZE,
        String MAIL_SMTP_HOST,
        String MAIL_SMTP_PORT,
        String MAIL_SMTP_PASSWORD,
        String MAIL_ADDRESS,
        String MAIL_COOLDOWN_PERIOD_MILLIS) {
}
