package ch.students4students;

import ch.kalajdzic.services.rest.RestServer;
import ch.kalajdzic.services.rest.config.ConfigReader;

import java.io.IOException;
import java.util.logging.LogManager;

public class Main {

    static {
        System.setProperty("java.util.logging.manager", WithShutdownLogManager.class.getName());
    }

    @Deprecated
    public static RestServer TheRestServer;

    public static void main(String[] args) throws ReflectiveOperationException, IOException {
        final S4SConfig config = ConfigReader.readConfig(S4SConfig.class, "s4s.properties");
        final RestServer<Server> server = new RestServer<>(new Server(config));
        server.start(Integer.parseInt(config.SERVER_PORT()), Integer.parseInt(config.SERVER_MAX_CONNECTIONS()));
        TheRestServer = server;
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                server.stop(10);
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                WithShutdownLogManager.instance.realReset();
            }
        }));
    }

    public static class WithShutdownLogManager extends LogManager {
        static WithShutdownLogManager instance;

        public WithShutdownLogManager() {
            instance = this;
        }

        @Override
        public void reset() {
        }

        private void realReset() {
            super.reset();
        }
    }
}
