package ch.students4students;

import ch.kalajdzic.services.rest.ContentTypes;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.kalajdzic.services.rest.annotations.Post;
import ch.students4students.data.Token;
import ch.students4students.data.ids.AccountId;
import ch.students4students.database.S4SDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.CodeSource;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HexFormat;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ch.students4students.LoginAPI.SESSION_TOKEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

@Path("/api/admin")
public class AdminPanel {

    public static final String PASSWORD_HASH_ALGORITHM = "SHA-256";
    public static final String PASSWORD_HEADER_NAME = "Admin-Password";
    private static final Logger logger = Logger.getLogger(AdminPanel.class.getName());


    private final ParticipantAPI participantAPI;
    private final byte[] passwordHash;
    //    private final ObjectWriter formWriter = Utils.objectMapper.writerFor(new TypeReference<List<ParticipantAPI.InscriptionFormRequest>>() {
//    });
    private final S4SDatabase database;
    private final AccountManager accountManager;
    private final AccountId admin;
    private final Server server;

    public AdminPanel(
            ParticipantAPI participantAPI,
            String passwordHashHex, S4SDatabase database, AccountManager accountManager,
            AccountId admin, Server server) {
        this.passwordHash = HexFormat.of().parseHex(passwordHashHex);
        this.participantAPI = participantAPI;
        this.database = database;
        this.accountManager = accountManager;
        this.admin = admin;
        this.server = server;
    }

    //
//    @Post
//    @Path("/createRoles")
//    @ContentType(ContentTypes.PLAIN_TEXT)
//    public ResponseConsumer createRoles() {
//        return chainVerification(e -> {
//            discordBot.createRoles(Arrays.stream(CourseEnum.values()).map(CourseEnum::frenchName).toList());
//            final StringBuilder b = new StringBuilder();
//            for (Course course : coursesRepository.getAll()) {
//                String username = accountsRepository.findById(course.rc()).orElseThrow().discord();
//                String role = course.id().value();
//                if (discordBot.giveRole(username, role)) {
//                    b.append("SUCCESS: Role ").append(role).append(" has been added to ").append(username).append('\n');
//                } else {
//                    b.append("FAILURE: Role ").append(role).append(" has not been added to ").append(username).append('\n');
//                }
//            }
//            b.append("DONE");
//            e.setResponseBody(b.toString());
//        });
//    }
//
//    @Post
//    @Path("/deleteRoles")
//    @ContentType(ContentTypes.PLAIN_TEXT)
//    public ResponseConsumer deleteRoles() {
//        return chainVerification(e -> {
//            final StringBuilder b = new StringBuilder();
//            for (Course course : coursesRepository.getAll()) {
//                String role = course.id().value();
//                discordBot.deleteRole(role);
//                b.append(role).append('\n');
//            }
//            b.append("DONE");
//            e.setResponseBody(b.toString());
//        });
//    }
    @Get
    @Path("/flushCache")
    @ContentType(ContentTypes.JSON)
    public ResponseConsumer flushCache() {
        return chainVerification(e -> {
            database.flushCache();
            e.setResponseBody("SUCCESS");
        });
    }

    @Get
    @Path("/whatJar")
    @Deprecated
    public ResponseConsumer whatJar() {
        return chainVerification(e -> {
            final CodeSource codeSource = Main.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            e.setResponseBody("SUCCESS:" + jarFile.getName() + ":" + new Date(jarFile.lastModified()));
        });
    }

    @Post
    @Path("/setJar")
    @Deprecated
    public ResponseConsumer setJar() {
        /**
         * Okay, hmm Let me cook
         * There's a high-level side effect of running this project that it may replace itself
         * It is normal if you don't find that normal.
         * So all in all it really is normal
         */
        return chainVerification(e -> {
            final byte[] theJarOhYeah = e.getRequestBody();
            logger.severe("Writing new JAR");

            try (OutputStream stream = new FileOutputStream("latest.jar")) {
                stream.write(theJarOhYeah);
            }
            logger.severe("New JAR written");
            new Thread(() -> {
                try {
                    logger.severe("Restarting server");
                    try {
                        Main.TheRestServer.stop(5);
                        e.setResponseBody("FAILURE");
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                    logger.info("Launching jar");
                    try {
                        Runtime.getRuntime().exec("java -jar latest.jar");
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                    logger.info("Jar launched");
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "an exception was thrown while restarting the server", ex);
                }
            }).start();
            e.setResponseBody("Result unknown");
        });
    }

//    @Get
//    @Path("/allForm")
//    @ContentType(ContentTypes.JSON)
//    public ResponseConsumer getInscriptions() {
//        return chainVerification(e -> e.setResponseBody(formWriter.writeValueAsBytes(participantAPI.inscriptions)));
//    }

    private ResponseConsumer chainVerification(ResponseConsumer chain) {
        return (e) -> {
            Optional<String> header = e.getHeader(PASSWORD_HEADER_NAME);
            if (header.isEmpty()) {
                if (admin == null) {
                    e.setError(HTTP_UNAUTHORIZED);
                    return;
                }
                Optional<Token> sessionToken = e.getHeader(SESSION_TOKEN)
                        .flatMap(accountManager::isAuthenticated);
                if (sessionToken.isPresent() && sessionToken.get().accountId().equals(admin)) {
                    chain.apply(e);
                    return;
                }
                e.setError(HTTP_UNAUTHORIZED);
                return;
            }
            final MessageDigest digest = MessageDigest.getInstance(PASSWORD_HASH_ALGORITHM);
            byte[] hash = digest.digest(Base64.getDecoder().decode(header.get().getBytes()));
            if (Arrays.equals(hash, passwordHash)) {
                chain.apply(e);
                return;
            }
            e.setError(HTTP_UNAUTHORIZED);
        };
    }
}
