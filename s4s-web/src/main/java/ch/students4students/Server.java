package ch.students4students;

import ch.kalajdzic.services.rest.annotations.Child;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.students4students.data.Account;
import ch.students4students.data.PersonalMail;
import ch.students4students.database.S4SDatabase;

import java.io.Closeable;
import java.time.Clock;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ch.students4students.data.AccountRole.ADMIN;

@Path("/")
public class Server implements Closeable {

    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private final S4SDatabase database;
    private final AdminPanel adminPanel;
    private final DiscordBot discordBot;
    private final TelegramBot telegramBot;
    private final AccountManager accountManager;
    private final MailManager mailManager;
    private final LoginAPI loginAPI;
    private final AccountAPI accountAPI;
    private final ParticipantAPI participantAPI;
    private final QRCodeAPI qrCodeAPI;
    private final WorkshopsAPI workshopsAPI;

    public Server(S4SConfig config) {
        this.database = new S4SDatabase(config.DATABASE_SERVER(),
                config.DATABASE_PORT(), config.DATABASE_NAME(),
                config.DATABASE_USER(), config.DATABASE_PASSWORD(),
                Integer.parseInt(config.DATABASE_MAX_CONNECTION()),
                Integer.parseInt(config.DATABASE_CACHE_SIZE()), Clock.systemUTC());
        this.database.init();
        this.discordBot = new DiscordBot(config.DISCORD_APP_TOKEN());
        final Optional<Account> admin = database.accountsRepository().findAllWithRole(ADMIN).stream().findFirst();
        final Long adminTelegramId = admin.filter(Account::verifiedTelegram).flatMap(Account::telegramId).orElse(null);
        this.telegramBot = new TelegramBot(config.TELEGRAM_BOT_TOKEN(), adminTelegramId);
        this.participantAPI = new ParticipantAPI();
        this.accountManager = new AccountManager(database.accountsRepository(), database.tokensRepository(), discordBot, telegramBot);
        this.mailManager = new MailManager(config.MAIL_SMTP_HOST(), config.MAIL_SMTP_PORT(),
                new PersonalMail(config.MAIL_ADDRESS()), config.MAIL_SMTP_PASSWORD(),
                Integer.parseInt(config.MAIL_COOLDOWN_PERIOD_MILLIS()));
        this.adminPanel = new AdminPanel(participantAPI, config.ADMIN_PASSWORD_HASH_HEX(), database, accountManager, admin.map(Account::id).orElse(null), this);
        this.loginAPI = new LoginAPI(accountManager, database.accountsRepository(), mailManager);
        this.accountAPI = new AccountAPI(database.accountsRepository(), loginAPI);
        this.qrCodeAPI = new QRCodeAPI(loginAPI, database.rolesRepository(), database.qrCodeRepository(), database.participantsRepository());
        this.workshopsAPI = new WorkshopsAPI(loginAPI, database.accountsRepository(), database.workshopRepository());
    }

    @Child
    public WebPages webPages() {
        return new WebPages();
    }

    @Child
    public LoginAPI loginAPI() {
        return loginAPI;
    }

    @Child
    public AdminPanel adminPanel() {
        return adminPanel;
    }

    @Child
    public AccountAPI accountAPI() {
        return accountAPI;
    }

    @Child
    public ParticipantAPI participantAPI() {
        return participantAPI;
    }

    @Child
    public QRCodeAPI qrCodeAPI() {
        return qrCodeAPI;
    }

    @Child
    public WorkshopsAPI workshopsAPI() {
        return workshopsAPI;
    }

    @Override
    public void close() {
        try {
            database.close();
        } finally {
            try {
                discordBot.shutdown();
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            } finally {
                try {
                    telegramBot.close();
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }
}
