package ch.students4students;

import ch.kalajdzic.services.rest.ContentTypes;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.kalajdzic.services.rest.annotations.Post;
import ch.students4students.data.AccountRole;
import ch.students4students.data.AccountRoles;
import ch.students4students.data.Participant;
import ch.students4students.data.QRCode;
import ch.students4students.data.ids.QRCodeValue;
import ch.students4students.database.ParticipantsRepository;
import ch.students4students.database.QRCodeRepository;
import ch.students4students.database.RolesRepository;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.Optional;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

@Path("/api/qrcode")
public class QRCodeAPI {

    private final LoginAPI loginAPI;
    private final RolesRepository rolesRepository;
    private final QRCodeRepository qrCodeRepository;
    private final ParticipantsRepository participantsRepository;
    private final ObjectWriter writer = Utils.objectMapper.writerFor(VerificationResponse.class);

    public QRCodeAPI(LoginAPI loginAPI, RolesRepository rolesRepository, QRCodeRepository qrCodeRepository, ParticipantsRepository participantsRepository) {
        this.loginAPI = loginAPI;
        this.rolesRepository = rolesRepository;
        this.qrCodeRepository = qrCodeRepository;
        this.participantsRepository = participantsRepository;
    }

    @Post
    @Path("/verify")
    @ContentType(ContentTypes.JSON)
    public ResponseConsumer verify() {
        return loginAPI.chainLogin((e, accountId) -> {
            AccountRoles accountRoles = rolesRepository.findById(accountId).orElseThrow();
            if (!accountRoles.roles().contains(AccountRole.BOARD_2024)) {
                e.setError(HTTP_UNAUTHORIZED);
                return;
            }

            final String code = e.getRequestBodyAsString();
            if (!QRCodeValue.isValid(code)) {
                e.setResponseBody("FAILURE");
                return;
            }
            Optional<QRCode> value = qrCodeRepository.findByValue(new QRCodeValue(code));
            if (value.isEmpty()) {
                e.setResponseBody("NOT FOUND");
                return;
            }
            qrCodeRepository.markAccess(value.get());
            final Participant participant = participantsRepository.findById(value.get().accountId()).orElseThrow();
            e.setResponseBody(writer.writeValueAsBytes(new VerificationResponse(value.get(), participant)));
        });
    }

    @Get
    @Path("/myCode")
    @ContentType(ContentTypes.PLAIN_TEXT)
    public ResponseConsumer myCode() {
        return loginAPI.chainLogin((e, accountId) -> {
            AccountRoles accountRoles = rolesRepository.findById(accountId).orElseThrow();
            if (!accountRoles.roles().contains(AccountRole.WEEK_PARTICIPANT_2024)) {
                e.setError(HTTP_UNAUTHORIZED);
                return;
            }
            final QRCode code = qrCodeRepository.findByAccountId(accountId, QRCode.QRCodePurpose.S4S_2024_WEEK_PARTICIPANT)
                    .orElseGet(() -> qrCodeRepository.create(accountId, QRCode.QRCodePurpose.S4S_2024_WEEK_PARTICIPANT));
            e.setResponseBody(code.value().value());
        });
    }

    public record VerificationResponse(long accessCount, long lastAccess, String bandColor, String welcomeRoom,
                                       String specialNotes) {
        VerificationResponse(QRCode qrCode, Participant participant) {
            this(qrCode.accessCount(), qrCode.lastAccess().toEpochMilli(), participant.bandColor(), participant.welcomeRoom(), participant.specialNotes());
        }
    }
}
