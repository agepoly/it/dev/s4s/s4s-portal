package ch.students4students;

import ch.kalajdzic.services.rest.ContentTypes;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Path;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.util.Objects.requireNonNull;

@Path("/")
public class WebPages {

    private static final Logger logger = Logger.getLogger(WebPages.class.getName());
    private final Map<String, byte[]> files = new HashMap<>();
    private final Map<String, String> guessedTypes = new HashMap<>();

    public WebPages() {
        try {
            load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load() throws IOException {
        String[] webFiles = new String(readResource("web-files.txt")).split("\\|");
        for (String file : webFiles) {
            final String guess = URLConnection.guessContentTypeFromName(file);
            if (guess != null) {
                guessedTypes.put(file, guess);
            }
            logger.log(Level.FINEST, "Load " + file + " in cache");
            files.put(file, readResource("web" + file));
        }
    }

    private byte[] readResource(String name) throws IOException {
        try (InputStream s = getClass().getClassLoader().getResourceAsStream(name)) {
            if (s == null) {
                System.out.println(name + " not found");
            }
            return requireNonNull(s).readAllBytes();
        }
    }

    @Get()
    @Path("/")
    public ResponseConsumer index() {
        return file("/index.html");
    }

    @Get()
    @Path("/favicon.ico")
    @ContentType(ContentTypes.PNG)
    public ResponseConsumer favicon() {
        return file("/favicon.ico");
    }

    @Get()
    @Path("/rc/staff")
    public ResponseConsumer staff() {
        return index();
    }

    @Get()
    @Path("/rc/archives")
    public ResponseConsumer archives() {
        return index();
    }

    @Get()
    @Path("/login")
    public ResponseConsumer login() {
        return index();
    }

    @Get()
    @Path("/inscription")
    public ResponseConsumer inscription() {
        return index();
    }

    @Get()
    @Path("/qrcode")
    public ResponseConsumer qrcode() {
        return index();
    }

    @Get()
    @Path("/workshops")
    public ResponseConsumer workshops() {
        return index();
    }

    private ResponseConsumer file(String filename) {
        return e -> {
            byte[] file = files.get(filename);
            if (file != null) {
                if (e.getResponseType().isEmpty()) {
                    e.setResponseType(guessedTypes.get(filename));
                }
                e.setResponseBody(file);
            } else {
                e.setError(HTTP_NOT_FOUND);
            }
        };
    }

    @Get()
    @Path("/assets/$filename")
    public ResponseConsumer asset(String filename) {
        return file("/assets/" + filename);
    }

}
