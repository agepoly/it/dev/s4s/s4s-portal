package ch.students4students;

import ch.kalajdzic.services.rest.EasyHttpExchange;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.kalajdzic.services.rest.annotations.Post;
import ch.students4students.data.Account;
import ch.students4students.data.DiscordUsername;
import ch.students4students.data.PersonalMail;
import ch.students4students.data.TelegramUsername;
import ch.students4students.data.Token;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Key;
import ch.students4students.database.AccountsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static ch.kalajdzic.services.rest.ContentTypes.PLAIN_TEXT;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

@Path("/api/login")
public class LoginAPI {

    public static final String SESSION_TOKEN = "Session-Token";
    private static final Logger log = LoggerFactory.getLogger(LoginAPI.class);
    private final AccountManager accountManager;
    private final AccountsRepository accountsRepository;
    private final MailManager mailManager;

    public LoginAPI(AccountManager accountManager, AccountsRepository accountsRepository, MailManager mailManager) {
        this.accountManager = accountManager;
        this.accountsRepository = accountsRepository;
        this.mailManager = mailManager;
    }

    @Post
    @Path("/discord/$username/sendCode")
    public ResponseConsumer discordSendCode(String username) {
        return e -> {
            if (!DiscordUsername.isValid(username)) {
                e.setResponseBody("INVALID");
            } else if (!accountManager.sendDiscordCode(new DiscordUsername(username))) {
                e.setResponseBody("NOT FOUND");
            } else {
                e.setResponseBody("SUCCESS");
            }
        };
    }

    @Post
    @Path("/discord/$username/login")
    @ContentType(PLAIN_TEXT)
    public ResponseConsumer discordLogin(String username) {
        return e -> {
            Optional<Token> token = accountManager.discordLogin(new DiscordUsername(username), e.getRequestBodyAsString());
            if (token.isPresent()) {
                e.setResponseBody("TOKEN:" + token.get().value());
            } else {
                e.setResponseBody("FAILURE");
            }
        };
    }

    @Post
    @Path("/telegram/$username/sendCode")
    public ResponseConsumer telegramSendCode(String username) {
        return e -> {
            if (!TelegramUsername.isValid(username)) {
                e.setResponseBody("INVALID");
            } else if (!accountManager.sendTelegramCode(new TelegramUsername(username))) {
                e.setResponseBody("NOT FOUND");
            } else {
                e.setResponseBody("SUCCESS");
            }
        };
    }

    @Post
    @Path("/telegram/$username/login")
    @ContentType(PLAIN_TEXT)
    public ResponseConsumer telegramLogin(String username) {
        return e -> {
            Optional<Token> token = accountManager.telegramLogin(new TelegramUsername(username), e.getRequestBodyAsString());
            if (token.isPresent()) {
                e.setResponseBody("TOKEN:" + token.get().value());
            } else {
                e.setResponseBody("FAILURE");
            }
        };
    }

    @Post
    @Path("/mail/$mail/sendCode")
    public ResponseConsumer mailSendCode(String mail) {
        return e -> {
            if (!PersonalMail.isValid(mail)) {
                e.setResponseBody("INVALID");
            } else if (!accountManager.sendMailCode(new PersonalMail(mail))) {
                e.setResponseBody("NOT FOUND");
            } else {
                e.setResponseBody("SUCCESS");
            }
        };
    }

    @Post
    @Path("/mail/$mail/login")
    @ContentType(PLAIN_TEXT)
    public ResponseConsumer mailLogin(String mail) {
        return e -> {
            Optional<Token> token = accountManager.mailLogin(new PersonalMail(mail), e.getRequestBodyAsString());
            if (token.isPresent()) {
                e.setResponseBody("TOKEN:" + token.get().value());
            } else {
                e.setResponseBody("FAILURE");
            }
        };
    }

    @Post
    @Path("/key/login")
    @ContentType(PLAIN_TEXT)
    public ResponseConsumer keyLogin() {
        return e -> {
            String requestBody = e.getRequestBodyAsString();
            if (!Key.isValid(requestBody)) {
                e.setResponseBody("INVALID");
                return;
            }
            Optional<Token> token = accountManager.keyLogin(new Key(requestBody));
            if (token.isPresent()) {
                e.setResponseBody("TOKEN:" + token.get().value());
            } else {
                e.setResponseBody("FAILURE");
            }
        };
    }

    @Post
    @Path("/key/lost")
    @ContentType(PLAIN_TEXT)
    public ResponseConsumer keyLost() {
        return e -> {
            String requestBody = e.getRequestBodyAsString();
            if (!PersonalMail.isValid(requestBody)) {
                e.setResponseBody("INVALID");
                return;
            }
            Optional<Account> account = accountsRepository.findByPersonalMail(new PersonalMail(requestBody));//TODO EPFL Mail?
            if (account.isPresent()) {
                mailManager.sendLostKey(new PersonalMail(requestBody), account.get().key());
                e.setResponseBody("SUCCESS:" + mailManager.averageTime());
                return;
            }
            log.info("Account not found : {}", requestBody);
            e.setResponseBody("NOT FOUND");
        };
    }

    public ResponseConsumer chainLogin(LoggedResponseConsumer chain) {
        return e -> {
            Optional<Token> sessionToken = e.getHeader(SESSION_TOKEN)
                    .flatMap(accountManager::isAuthenticated);
            if (sessionToken.isPresent()) {
                chain.apply(e, sessionToken.get().accountId());
            } else {
                e.setError(HTTP_UNAUTHORIZED);
            }
        };
    }

    public interface LoggedResponseConsumer {
        void apply(EasyHttpExchange e, AccountId account) throws Exception;
    }
}
