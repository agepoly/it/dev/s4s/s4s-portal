package ch.students4students;

import ch.kalajdzic.services.rest.ContentTypes;
import ch.kalajdzic.services.rest.ResponseConsumer;
import ch.kalajdzic.services.rest.annotations.ContentType;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.kalajdzic.services.rest.annotations.Post;
import ch.students4students.data.Account;
import ch.students4students.data.AccountWorkshops;
import ch.students4students.data.WorkshopEntry;
import ch.students4students.database.AccountsRepository;
import ch.students4students.database.WorkshopRepository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/api/workshops")
public class WorkshopsAPI {

    private final LoginAPI loginAPI;
    private final AccountsRepository accountsRepository;
    private final WorkshopRepository workshopRepository;


    public WorkshopsAPI(LoginAPI loginAPI, AccountsRepository accountsRepository, WorkshopRepository workshopRepository) {
        this.loginAPI = loginAPI;
        this.accountsRepository = accountsRepository;
        this.workshopRepository = workshopRepository;
    }

    @Get
    @Path("/me")
    @ContentType(ContentTypes.JSON)
    public ResponseConsumer forMe() {
        return loginAPI.chainLogin((e, account) -> {
            AccountWorkshops workshops = workshopRepository.findById(account).orElseGet(() -> workshopRepository.createFor(new AccountWorkshops(account, Set.of())));
            List<String> onlyIds = workshops.workshops().stream().map(WorkshopEntry::id).toList();
            e.setResponseBody(Utils.objectMapper.writeValueAsBytes(onlyIds));
        });
    }

    @Get
    @Path("/max")
    @ContentType(ContentTypes.JSON)
    public ResponseConsumer max() {
        return loginAPI.chainLogin((e, account) -> {
            final Map<String, Boolean> map = new HashMap<>();
            //TODO this is super slow -> we read the whole database N times!!
            for (WorkshopEntry workshop : WorkshopEntry.ALL_WORKSHOPS) {
                map.put(workshop.id(), workshopRepository.findAllIn(workshop).size() < WorkshopEntry.max(workshop));
            }
            e.setResponseBody(Utils.objectMapper.writeValueAsBytes(map));
        });
    }

    @Get
    @Path("/registrations/$id")
    @ContentType("text/csv")
    public ResponseConsumer registrations(String id) {
        return e -> {
            List<AccountWorkshops> all = workshopRepository.findAllIn(new WorkshopEntry(id));
            List<Account> accounts = all.stream()
                    .map(workshops -> accountsRepository.findById(workshops.id()).orElseThrow())
                    .toList();

            e.setResponseBody("prénom,nom,email\n" +
                    accounts.stream()
                            .map(account -> account.firstName() + "," + account.lastName() + "," + account.personalMail().orElseThrow().value())
                            .collect(Collectors.joining("\n")));
        };
    }

    @Post
    @Path("/register/$id")
    @ContentType(ContentTypes.PLAIN_TEXT)
    public ResponseConsumer register(String id) {
        return loginAPI.chainLogin((e, account) -> {
            WorkshopEntry entry = new WorkshopEntry(id);
            int size = workshopRepository.findAllIn(entry).size();
            if (size >= WorkshopEntry.max(entry)) {
                e.setResponseBody("FAILED");
                return;
            }
            AccountWorkshops workshops = workshopRepository.findById(account).orElseThrow();
            Set<WorkshopEntry> newWorkshops = new HashSet<>(workshops.workshops());
            newWorkshops.add(entry);
            workshopRepository.update(workshops, new AccountWorkshops(account, newWorkshops));
            e.setResponseBody("SUCCESS");
        });
    }

    @Post
    @Path("/unregister/$id")
    @ContentType(ContentTypes.PLAIN_TEXT)
    public ResponseConsumer unregister(String id) {
        return loginAPI.chainLogin((e, account) -> {
            WorkshopEntry entry = new WorkshopEntry(id);
            AccountWorkshops workshops = workshopRepository.findById(account).orElseThrow();
            Set<WorkshopEntry> newWorkshops = new HashSet<>(workshops.workshops());
            newWorkshops.remove(entry);
            workshopRepository.update(workshops, new AccountWorkshops(account, newWorkshops));
            e.setResponseBody("SUCCESS");
        });
    }


}
