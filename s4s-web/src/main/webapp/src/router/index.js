import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ParticipationView from "@/views/ParticipationView.vue";
import {isLogged} from "@/common/ServerAPI.js";
import LoginView from "@/views/LoginView.vue";
import ArchivesView from "@/views/ArchivesView.vue";
import StaffView from "@/views/StaffView.vue";
import CheckQRCode from "@/views/CheckQRCode.vue";
import WorkshopsView from "@/views/WorkshopsView.vue";

/**
 * Don't forget to update ch.students4students.WebPages accordingly ! (in dev mode it won't matter)
  */
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView,
        },
        {
            path: '/inscription',
            name: 'inscription',
            component: ParticipationView
        },
        {
            path: '/rc/staff',
            name: 'staff',
            component: StaffView
        },
        {
            path: '/rc/archives',
            name: 'archives',
            component: ArchivesView
        },
        {
            path: '/login',
            name: 'login',
            component: LoginView
        },
        {
            path: '/qrcode',
            name: 'qrcode',
            component: CheckQRCode
        },
        {
            path: '/workshops',
            name: 'workshops',
            component: WorkshopsView
        }
    ]
})

router.beforeEach(async (to, from) => {
    if (!isLogged() && to.name !== 'login') {
        return '/login';
    }
    if (isLogged() && to.name === 'login') {
        return '/';
    }
})

export default router
