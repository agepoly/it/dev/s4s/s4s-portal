export const GET = "GET";
export const POST = "POST";

const JSON_TYPE = "application/json; charset=utf-8";
const TEXT_TYPE = "text/plain; charset=utf-8";
const APPLICATION_OCTET_STREAM = "application/octet-stream";

let token = localStorage.getItem("token");

export function reloadToken() {
    token = localStorage.getItem("token");
}

export function isLogged() {
    return token != null;
}

let alertFun = alert;

export function setAlertFun(fun) {
    alertFun = fun;
}

export function myAlert(message) {
    alertFun(message);
}

export function genericError() {
    alertFun("Une erreur s'est produite !");
}

export function request(method, url, onSuccess, onFailure, contentType = undefined, data = undefined) {
    const r = new XMLHttpRequest();
    // if (import.meta.env.MODE === "development") {
    //     url = "http://localhost:8080" + url;
    // }
    r.open(method, url);
    r.setRequestHeader("Content-Type", contentType);
    if (token != null) {
        r.setRequestHeader("Session-Token", token);
    }
    r.onreadystatechange = () => {
        if (r.readyState === XMLHttpRequest.DONE) {
            if (r.status === 200) {
                onSuccess(r.responseText);
            } else if (r.status === 401) {
                localStorage.clear()
                window.location = "/login";
            } else {
                onFailure(r.status, r.responseText);
            }
        }
    };
    r.send(data);
}

export function discordSendCode(username, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/discord/" + window.encodeURIComponent(username) + "/sendCode", onSuccess, onFailure);
}

export function discordLogin(username, code, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/discord/" + window.encodeURIComponent(username) + "/login", onSuccess, onFailure, TEXT_TYPE, code);
}

export function telegramSendCode(username, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/telegram/" + window.encodeURIComponent(username) + "/sendCode", onSuccess, onFailure);
}

export function telegramLogin(username, code, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/telegram/" + window.encodeURIComponent(username) + "/login", onSuccess, onFailure, TEXT_TYPE, code);
}

export function keyLogin(key, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/key/login", onSuccess, onFailure, TEXT_TYPE, key);
}

export function mailSendCode(mail, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/mail/" + window.encodeURIComponent(mail) + "/sendCode", onSuccess, onFailure);
}

export function mailLogin(mail, code, onSuccess, onFailure = genericError) {
    request(POST, "/api/login/mail/" + window.encodeURIComponent(mail) + "/login", onSuccess, onFailure, TEXT_TYPE, code);
}


export function getUserData(onSuccess, onFailure = genericError) {
    return request(GET, "/api/account/basic", onSuccess, onFailure);
}

export function getRCCourse(onSuccess, onFailure = genericError) {
    return request(GET, "/api/account/rc/courseName", onSuccess, onFailure);
}

export function getCandidates(onSuccess, onFailure = genericError) {
    return request(GET, "/api/account/rc/candidates", onSuccess, onFailure);
}

export function getChoices(onSuccess, onFailure = genericError) {
    return request(GET, "/api/account/rc/choices", onSuccess, onFailure);
}

export function setChoices(onSuccess, onFailure = genericError, choices) {
    return request(POST, "/api/account/rc/choices", onSuccess, onFailure, JSON_TYPE, choices);
}

export function inscriptionForm(onSuccess, onFailure = genericError, form) {
    return request(POST, "/api/inscriptionForm", onSuccess, onFailure, JSON_TYPE, form);
}

export function adminFlushCache(onSuccess, onFailure = genericError) {
    return request(GET, "/api/admin/flushCache", onSuccess, onFailure);
}

export function verifyQrCode(code, onSuccess, onFailure = genericError) {
    return request(POST, "/api/qrcode/verify", onSuccess, onFailure, TEXT_TYPE, code);
}

export function myQrCode(onSuccess, onFailure = genericError) {
    return request(GET, "/api/qrcode/myCode", onSuccess, onFailure);
}

export function lostKey(mail, onSuccess, onFailure = genericError) {
    return request(POST, "/api/login/key/lost", onSuccess, onFailure, TEXT_TYPE, mail);
}

export function workshopsMe(onSuccess, onFailure = genericError) {
    return request(GET, "/api/workshops/me", onSuccess, onFailure);
}

export function workshopsMax(onSuccess, onFailure = genericError) {
    return request(GET, "/api/workshops/max", onSuccess, onFailure);
}

export function workshopsRegister(workshopId, onSuccess, onFailure = genericError) {
    return request(POST, "/api/workshops/register/"+workshopId, onSuccess, onFailure);
}

export function workshopsUnregister(workshopId, onSuccess, onFailure = genericError) {
    return request(POST, "/api/workshops/unregister/"+workshopId, onSuccess, onFailure);
}