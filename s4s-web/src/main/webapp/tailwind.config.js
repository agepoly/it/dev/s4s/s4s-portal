/** @type {import('tailwindcss').Config} */

export default {
    darkMode: 'class',
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
        "./node_modules/flowbite/**/*.js"
    ],
    theme: {
        colors: {
            white: '#ffffff',
            black: '#000000',
            s4s: {
                purple: {
                    50: '#ebecf6',
                    100: '#d7d8ec',
                    200: '#b9bcdd',
                    300: '#999dcc',
                    400: '#999dcc',
                    500: '#7e83b6',
                    600: '#6368a4',
                    700: '#525684',
                    800: '#424469',
                    900: '#33355a',
                },
                redwood: {
                    50: '#fbf9f6',
                    100: '#fbf9f6',
                    200: '#f7f3ee',
                    300: '#f3eee6',
                    400: '#ede3db',
                    500: '#e7d5c6',
                    600: '#e1c2a7',
                    700: '#d3a589',
                    800: '#a5605e',
                    900: '#824240'
                }
            }
        },
    },
    plugins: [
        import('flowbite/plugin')
    ]
}

