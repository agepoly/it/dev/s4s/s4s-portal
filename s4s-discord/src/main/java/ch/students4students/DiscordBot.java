package ch.students4students;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.channel.concrete.Category;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.entities.channel.middleman.GuildChannel;
import net.dv8tion.jda.api.events.guild.GuildReadyEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

import java.awt.*;
import java.time.Duration;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;

public class DiscordBot {

    private final Random random = new Random();
    private JDA jda;
    private Guild s4sGuild = null;
    private List<Member> members;

    public DiscordBot(String token) {
        jda = JDABuilder.createLight(token, EnumSet.of(GatewayIntent.GUILD_MEMBERS, GatewayIntent.DIRECT_MESSAGES))
                .addEventListeners(new DiscordEventListener())
                .build();
    }

    public void sendCode(long user, String code) {
        openPrivateChannel(user, channel -> channel.sendMessage("""
                Login code: `%s`. This code is valid for 5 minutes.

                Do not give this code to anyone, even if they say they are from S4S!
                This code can be used to log in to your S4S account. We never ask it for anything else.

                If you didn't request this code by trying to log in to your S4S account, please contact us at s4s@epfl.ch
                """.formatted(code)).queue());
    }

    private void openPrivateChannel(long user, Consumer<PrivateChannel> consumer) {
        for (PrivateChannel privateChannel : jda.getPrivateChannels()) {
            if (Objects.requireNonNull(privateChannel.getUser()).getIdLong() == user) {
                consumer.accept(privateChannel);
                return;
            }
        }
        s4sGuild.loadMembers().onSuccess(members -> {
            for (Member member : members) {
                if (member.getUser().getIdLong() == user) {
                    jda.openPrivateChannelById(member.getId()).queue(consumer);
                    return;
                }
            }
        });
    }

    public synchronized void shutdown() throws InterruptedException {
        if (jda == null) {
            System.out.println("JDA is already down");
            return;
        }
        System.out.println("Shutting down JDA");
        jda.shutdown();
        if (!jda.awaitShutdown(Duration.ofSeconds(10))) {
            System.out.println("Forcefully shutting down JDA");
            jda.shutdownNow();
            jda.awaitShutdown();
        }
        System.out.println("JDA has been shutdown");
        jda = null;
    }

    public void createRoles(List<String> roles) {
        roles.forEach(this::getOrCreateRole);
    }

    public boolean giveRole(String user, String role) {
        Role role1 = getOrCreateRole(role);
        List<Member> members = s4sGuild.loadMembers().get();
        for (Member member : members) {
            String name1 = member.getUser().getName();
            String name2 = member.getUser().getEffectiveName();
            String name3 = member.getUser().getGlobalName();

            if (user.equals(name1) || user.equals(name2) || user.equals(name3)) {
                if (!member.getRoles().contains(role1)) {
                    s4sGuild.addRoleToMember(member.getUser(), role1).complete();
                    System.out.println("Role " + role + " has been given to " + user);
                } else {
                    System.out.println("Role already present for " + user);
                }
                return true;
            }
        }
        System.out.println("Role " + role + " could not be given to " + user + ": the user was not found in the guild.");
        return false;
    }

    public Role getOrCreateRole(String name) {
        Optional<Role> role = s4sGuild.getRolesByName(name, false).stream().findAny();
        if (role.isPresent()) {
//            System.out.println("Role " + name + " already exists");
            return role.get();
        } else {
            System.out.println("Role " + name + " will be created");
            Color randomColor = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
            return s4sGuild.createRole().setColor(randomColor).setName(name).complete();
        }
    }

    public void giveCurieuxPerms(List<String> cats) {
        Role orCreateRole = getOrCreateRole("Curieux");
        for (String name : cats) {
            Optional<Category> cat = s4sGuild.getCategoriesByName(name, false).stream().findAny();
            if (cat.isEmpty()) {
                System.out.println("Category " + name + "does not exists");
            } else {
                cat.get().upsertPermissionOverride(orCreateRole).grant(Permission.VIEW_CHANNEL).complete();
                for (GuildChannel channel : cat.get().getChannels()) {
                    channel.getPermissionContainer().upsertPermissionOverride(orCreateRole).grant(Permission.VIEW_CHANNEL).complete();
                    System.out.println("Channel " + channel.getName() + " has been upgraded to " + name + " for cat " + cat.get().getName());
                }
            }
        }
    }

    public Category getOrCreateCategory(Role role, String name) {
//        List<Member> members = s4sGuild.getMembers();

        Optional<Category> cat = s4sGuild.getCategoriesByName(name, false).stream().findAny();
        if (cat.isPresent()) {
            System.out.println("Category " + name + " already exists");
            return cat.get();
        } else {
            Role rcrole = getOrCreateRole("RC");
            List<Member> theRCs = members.stream().filter(member -> member.getRoles().contains(role)).filter(member -> member.getRoles().contains(rcrole)).toList();
            if (theRCs.isEmpty()) {
                System.out.println("rc not found");
                System.out.println();
                return null;
            }
            System.out.println("Category " + name + " will be created");
            Category complete = s4sGuild.createCategory(name)
                    .addPermissionOverride(
                            theRCs.getFirst(),
                            List.of(Permission.MANAGE_CHANNEL), List.of())
                    .addPermissionOverride(role, List.of(Permission.VIEW_CHANNEL), List.of())
                    .addPermissionOverride(s4sGuild.getPublicRole(), List.of(), List.of(Permission.VIEW_CHANNEL))
                    .complete();
            complete.createTextChannel("general").complete();
            System.out.println("Category " + name + " has been created");
            return complete;
        }
    }

    public void deleteRole(String role) {
        s4sGuild.getRolesByName(role, false).stream().findAny().ifPresentOrElse(role1 -> {
            role1.delete().complete();
            System.out.println("Role " + role + " has been deleted");
        }, () -> System.out.println("Role " + role + " did not exist, cannot be deleted"));
    }

    public void createCategories(List<String> categories) {
        members = s4sGuild.loadMembers().get();
        categories.forEach(s -> getOrCreateCategory(getOrCreateRole(s), s));
    }

    public void giveStaffRole(List<String> roles) {
        members = s4sGuild.loadMembers().get();
        Role orCreateRole = getOrCreateRole("2024 Staff");
        for (Member member : members) {
            if (member.getRoles().stream().map(Role::getName).anyMatch(roles::contains)) {
                if (!member.getRoles().contains(orCreateRole)) {
                    System.out.println("Added staff role to " + member.getUser().getName());
                    s4sGuild.addRoleToMember(member, orCreateRole).complete();
                } else {
                    System.out.println("Role staff already present for " + member.getUser().getName());
                }
            } else {
                System.out.println("not staff");
            }
        }
    }

    private class DiscordEventListener extends ListenerAdapter {
        @Override
        public void onGuildReady(GuildReadyEvent event) {
            if (s4sGuild != null) {
                throw new RuntimeException("The bot should be present in one single guild only");
            }
            System.out.println("GOT GUILD");
            s4sGuild = event.getGuild();
        }

        @Override
        public void onMessageReceived(MessageReceivedEvent event) {
            System.out.println("ignore");
//            System.out.println(event.getChannelType());
//            if (event.getChannelType() == ChannelType.PRIVATE) {
//                event.getChannel().sendMessage("""
//                        Hello!
//                        I am the discord bot of S4S, I will be used to connect to your S4S account.
//
//                        Your discord ID is : `%d`
//                        """.formatted(event.getAuthor().getIdLong())).complete();
//            }
        }
    }
}
