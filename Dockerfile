FROM openjdk:21
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN --mount=type=cache,target=/root/.m2 ./mvnw clean package
EXPOSE 8080

ENTRYPOINT java -jar /app/s4s-web/target/s4s-web-jar-with-dependencies.jar
