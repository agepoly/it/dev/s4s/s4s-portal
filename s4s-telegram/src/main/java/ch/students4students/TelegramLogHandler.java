package ch.students4students;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramClient;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class TelegramLogHandler extends Handler {

    private final Long admin;
    private final TelegramClient client;

    public TelegramLogHandler(Long admin, TelegramClient client) {
        this.admin = admin;
        this.client = client;
        setFormatter(new MessageFormatter());
        if (admin == null) {
            setLevel(Level.OFF);
        }
    }

    @Override
    public void publish(LogRecord logRecord) {
        if (!isLoggable(logRecord) || admin == null) {
            return;
        }
        final String message = getFormatter().format(logRecord);
        final boolean silent = logRecord.getLevel().intValue() < Level.WARNING.intValue();
        try {
            client.execute(SendMessage.builder()
                    .chatId(admin)
                    .parseMode("markdown")
                    .text(message)
                    .disableNotification(silent)
                    .build());
        } catch (TelegramApiException e) {
            reportError("Failed to log message", e, ErrorManager.WRITE_FAILURE);
        }
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
        if (admin == null) {
            return;
        }
        try {
            client.execute(SendMessage.builder()
                    .chatId(admin)
                    .text("Server is closing. Bye! \uD83D\uDC4B")
                    .build());
        } catch (TelegramApiException e) {
            reportError("Failed to send final message", e, ErrorManager.FLUSH_FAILURE);
        }
    }

    private static class MessageFormatter extends SimpleFormatter {

        @Override
        public String format(LogRecord record) {
            String source;
            if (record.getSourceClassName() != null) {
                source = record.getSourceClassName();
                if (record.getSourceMethodName() != null) {
                    source = source + " " + record.getSourceMethodName();
                }
            } else {
                source = record.getLoggerName();
            }

            String message = formatMessage(record);
            String throwable = "";
            if (record.getThrown() != null) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                throwable = "\n```java\n" + sw + "```";
            }
            return """
                    _%s_
                    %s *%s*%s""".formatted(source, level(record.getLevel().intValue()), message, throwable);
        }

        private String level(int level) {
            if (level >= Level.SEVERE.intValue()) {
                return "\uD83D\uDD34";
            } else if (level >= Level.WARNING.intValue()) {
                return "⚠";
            } else if (level >= Level.INFO.intValue()) {
                return "ℹ️";
            } else if (level >= Level.CONFIG.intValue()) {
                return "⚙";
            } else if (level >= Level.FINE.intValue()) {
                return "\uD83D\uDD08";
            } else if (level >= Level.FINER.intValue()) {
                return "\uD83D\uDD09";
            } else if (level >= Level.FINEST.intValue()) {
                return "\uD83D\uDD0A";
            } else {
                return "\uD83D\uDD07";
            }
        }
    }
}
