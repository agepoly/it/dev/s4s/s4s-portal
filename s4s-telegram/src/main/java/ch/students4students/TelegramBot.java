package ch.students4students;

import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;
import org.telegram.telegrambots.longpolling.BotSession;
import org.telegram.telegrambots.longpolling.TelegramBotsLongPollingApplication;
import org.telegram.telegrambots.longpolling.util.LongPollingSingleThreadUpdateConsumer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramClient;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TelegramBot implements LongPollingSingleThreadUpdateConsumer, AutoCloseable {
    private static final Logger logger = Logger.getLogger(TelegramBot.class.getName());
    private final TelegramClient telegramClient;
    private final TelegramBotsLongPollingApplication api;
    private final BotSession session;
    private final TelegramLogHandler logHandler;

    public TelegramBot(String token, Long admin) {
        api = new TelegramBotsLongPollingApplication();
        try {
            session = api.registerBot(token, this);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
        telegramClient = new OkHttpTelegramClient(session.getOkHttpClient(), token);
        logHandler = new TelegramLogHandler(admin, telegramClient);
        Logger.getLogger("").addHandler(logHandler);
    }

    public void sendCode(long id, String code) {
        try {
            telegramClient.execute(SendMessage.builder().chatId(id).parseMode("markdown").text("""
                    Login code: `%s`. This code is valid for 4 minutes and 20 seconds.
                        
                    Do not give this code to anyone, even if they say they are from S4S!
                    This code can be used to log in to your S4S account. We never ask it for anything else.
                        
                    If you didn't request this code by trying to log in to your S4S account, please contact us at s4s@epfl.ch
                    """.formatted(code)).build());
        } catch (TelegramApiException e) {
            logger.log(Level.SEVERE, "An exception was thrown while trying to send a login code to " + code, e);
            throw new RuntimeException(e);
        }
    }

    public void close() throws Exception {
        try {
            Logger.getLogger("").removeHandler(logHandler);
            logHandler.close();
        } finally {
            try {
                session.close();
            } catch (TelegramApiException e) {
                logger.log(Level.SEVERE, "An exception was thrown while closing session", e);
            } finally {
                api.close();
            }
        }
    }

    @Override
    public void consume(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message_text = update.getMessage().getText();
            if (message_text.equals("/start")) {
                final long chat_id = update.getMessage().getChatId();
                final long userId = update.getMessage().getFrom().getId();

                final SendMessage message = SendMessage.builder()
                        .chatId(chat_id)
                        .parseMode("markdown")
                        .text("Votre ID telegram est: `" + userId + "`").build();
                try {
                    telegramClient.execute(message); // Sending our message object to user
                } catch (TelegramApiException e) {
                    logger.log(Level.SEVERE, "An exception was thrown while sending a message", e);
                }
            }
        }
    }
}
