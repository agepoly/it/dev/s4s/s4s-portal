//package ch.students4students.data;
//
//import com.fasterxml.jackson.databind.ObjectReader;
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//
//import java.sql.*;
//import java.time.Clock;
//import java.util.Arrays;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//
//@Deprecated(forRemoval = true)
//public class SQLDatabase implements AutoCloseable {
//
//    private static final String ACCOUNTS_TABLE_NAME = "accounts";
//    private static final String USER_ROLES_TABLE_NAME = "user_roles";
//    private static final String PERMISSIONS_TABLE_NAME = "permissions";
//    private static final String STAFF_CANDIDATURES_TABLE_NAME = "candidatures";
//    private static final String REGISTRATIONS_TABLE_NAME = "registrations";
//    private static final String LEGACY_FILES_TABLE_NAME = "files";
//
//    /**
//     * User account IDs start with this prefix.
//     * If changed, it must also be changed in the corresponding SQL table creation query.
//     */
//    private static final String ACCOUNTS_ID_PREFIX = "acc-";
//    /**
//     * User role IDs start with this prefix.
//     * If changed, it must also be changed in the corresponding SQL table creation query.
//     */
//    private static final String USER_ROLES_ID_PREFIX = "role-";
//    /**
//     * Staff candidature IDs start with this prefix.
//     * If changed, it must also be changed in the corresponding SQL table creation query.
//     */
//    private static final String STAFF_CANDIDATURES_ID_PREFIX = "can-";
//    /**
//     * Registration IDs start with this prefix.
//     * If changed, it must also be changed in the corresponding SQL table creation query.
//     */
//    private static final String REGISTRATIONS_ID_PREFIX = "reg-";
//
//    private static final Logger logger = Logger.getLogger(SQLDatabase.class.getName());
//    private final Clock clock;
//    private final HikariDataSource source;
//
//    public SQLDatabase(String server, int port, String databaseName, String user, String password, int maxConnections, Clock clock) {
//        this.clock = clock;
//        HikariConfig config = new HikariConfig();
//        config.setJdbcUrl("jdbc:postgresql://" + server + ":" + port + "/" + databaseName);
//        config.setUsername(user);
//        config.setPassword(password);
//        config.setMaximumPoolSize(maxConnections);
//        source = new HikariDataSource(config);
//    }
//
//    /**
//     * Detect whether the database is using the old JSON format or the new SQL format.
//     */
//    public `DatabaseVersion assessVersion() {
//        try (Connection connection = source.getConnection()) {
//            DatabaseMetaData dbMetaData = connection.getMetaData();
//
//            // See whether some proper SQL tables exist
//            try (ResultSet rs = dbMetaData.getTables(null, null, ACCOUNTS_TABLE_NAME, null)) {
//                if (rs.next()) {
//                    return DatabaseVersion.V1;
//                }
//            }
//
//            // Otherwise, see whether the table "files" exists
//            try (ResultSet rs = dbMetaData.getTables(null, null, LEGACY_FILES_TABLE_NAME, null)) {
//                if (rs.next()) {
//                    return DatabaseVersion.JSON_DATABASE;
//                }
//            }
//
//            // If none of those tables were found, we assume the database is empty
//            return DatabaseVersion.NO_DATABASE;
//        }
//        catch (SQLException e) {
//            logger.log(Level.SEVERE, e.getMessage(), e);
//            throw new RuntimeException(e);
//        }
//    }
//
//    public synchronized void createSqlTablesIfAbsent() {
//        try (Connection connection = source.getConnection()) {
//
//            // Create an SQL type for the section
//            String sections = Arrays.stream(Section.values()).map(Section::shortName).collect(Collectors.joining("', '"));
//            PreparedStatement createSectionType = connection.prepareStatement("CREATE TYPE SECTION AS ENUM ('" + sections + "');");
//            createSectionType.executeUpdate();
//
//            // Create an SQL type for the semester
//            String semesters = Arrays.stream(Semester.values()).map(Semester::shortName).collect(Collectors.joining("', '"));
//            PreparedStatement createSemesterType = connection.prepareStatement(
//                    "CREATE TYPE SEMESTER AS ENUM ('" + semesters + "');"
//            );
//            createSemesterType.executeUpdate();
//
//            // Create an SQL type for the doubling status
//            String doublingStatuses = Arrays.stream(DoublingStatus.values()).map(DoublingStatus::getFrenchText).collect(Collectors.joining("', '"));
//            PreparedStatement createDoublingStatusType = connection.prepareStatement(
//                    "CREATE TYPE DOUBLING_STATUS AS ENUM ('" + doublingStatuses + "');"
//            );
//
//            // Table to store the user accounts
//            PreparedStatement createAccountsTable = connection.prepareStatement("""
//                    CREATE TABLE IF NOT EXISTS ?(
//                        id VARCHAR NOT NULL,
//                        display_name VARCHAR NOT NULL,
//                        first_name VARCHAR NOT NULL,
//                        last_name VARCHAR NOT NULL,
//                        email VARCHAR,
//                        verified_email BOOLEAN NOT NULL,
//                        epfl_mail VARCHAR,
//                        verified_epfl_mail BOOLEAN NOT NULL,
//                        telegram VARCHAR,
//                        verified_telegram BOOLEAN NOT NULL,
//                        discord VARCHAR,
//                        verified_discord BOOLEAN NOT NULL,
//
//                        PRIMARY KEY (id),
//
//                        CHECK ( id LIKE 'acc-' ) -- Only allow IDs starting with 'acc-'
//                    );
//                    """
//            );
//            createAccountsTable.setString(1, ACCOUNTS_TABLE_NAME);
//            createAccountsTable.executeUpdate();
//
//            // Table to store the existing roles (without users yet)
//            PreparedStatement createUserRolesTable = connection.prepareStatement("""
//                    CREATE TABLE IF NOT EXISTS ?(
//                        id VARCHAR NOT NULL,
//                        french_name VARCHAR NOT NULL,
//
//                        PRIMARY KEY (id),
//                        UNIQUE (french_name), -- Two roles should not have the same readable name
//
//                        CHECK ( id LIKE 'role-' ) -- Only allow IDs starting with 'role-'
//                    );
//                    """
//            );
//            createUserRolesTable.setString(1, USER_ROLES_TABLE_NAME);
//            createUserRolesTable.executeUpdate();
//
//            // Table to associate users to their roles
//            PreparedStatement createUserRolesAssociationTable = connection.prepareStatement("""
//                    CREATE TABLE IF NOT EXISTS ?(
//                        account_id VARCHAR NOT NULL,
//                        role_id VARCHAR NOT NULL,
//
//                        PRIMARY KEY (account_id, role_id),
//
//                        FOREIGN KEY (account_id) REFERENCES ?(id),
//                        FOREIGN KEY (role_id) REFERENCES ?(id)
//                    );
//                    """
//            );
//            createUserRolesAssociationTable.setString(1, PERMISSIONS_TABLE_NAME);
//            createUserRolesAssociationTable.setString(2, ACCOUNTS_TABLE_NAME);
//            createUserRolesAssociationTable.setString(3, USER_ROLES_TABLE_NAME);
//            createUserRolesAssociationTable.executeUpdate();
//
//            // Existing courses are hard-coded in the corresponding Enum, no need for a table
//
//            // Table to store staff candidatures
//            PreparedStatement createStaffCandidaturesTable = connection.prepareStatement("""
//                    CREATE TABLE IF NOT EXISTS ?(
//                        id VARCHAR NOT NULL,
//                        account_id VARCHAR NOT NULL,
//                        section SECTION NOT NULL, -- IN, SC, AR, CMS, ...
//                        semester SEMESTER NOT NULL, -- BA1, BA2, BA3, BA4, BA5, BA6, MA1, MA2, MA3, MA4
//                        description TEXT NOT NULL,
//                        -- TODO : Generate a list of courses and three roles for each course somehow
//
//                        PRIMARY KEY (id),
//
//                        FOREIGN KEY (account_id) REFERENCES ?(id),
//                        CHECK ( id LIKE 'can-' ) -- Only allow IDs starting with 'can-'
//                    );
//                    """
//            );
//            createStaffCandidaturesTable.setString(1, STAFF_CANDIDATURES_TABLE_NAME);
//            createStaffCandidaturesTable.setString(2, ACCOUNTS_TABLE_NAME);
//            createStaffCandidaturesTable.executeUpdate();
//
//            // Table to store the participant registrations
//            PreparedStatement createParticipantRegistrationsTable = connection.prepareStatement("""
//                    CREATE TABLE IF NOT EXISTS ?(
//                        id VARCHAR NOT NULL,
//                        account_id VARCHAR NOT NULL, -- The user account of the participant
//                        section SECTION NOT NULL, -- IN, SC, AR, CMS, ...
//                        doubling_status DOUBLING_STATUS NOT NULL, -- Whether the participant is doubling or did CMS
//                        special BOOLEAN NOT NULL,
//                        remarks VARCHAR,
//
//                        PRIMARY KEY (id),
//
//                        FOREIGN KEY (account_id) REFERENCES ?(id),
//                        CHECK ( id LIKE 'reg-' ) -- Only allow IDs starting with 'reg-'
//                    );
//                    """
//            );
//            createParticipantRegistrationsTable.setString(1, REGISTRATIONS_TABLE_NAME);
//            createParticipantRegistrationsTable.setString(2, ACCOUNTS_TABLE_NAME);
//            createParticipantRegistrationsTable.executeUpdate();
//
//            // TODO : Generate tables for the selections that the RCs made
//
//            // TODO : Generate a table for the tokens
//        }
//        catch (SQLException e) {
//            logger.log(Level.SEVERE, e.getMessage(), e);
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public void close() throws SQLException {
//        source.close();
//    }
//}
