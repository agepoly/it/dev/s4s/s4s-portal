package ch.students4students;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Optional;

public class Utils {

    public static final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

    public static Optional<Long> getOptionalLong(ResultSet res, String s) throws SQLException {
        final long l = res.getLong(s);
        return res.wasNull() ? Optional.empty() : Optional.of(l);
    }

    public record SQLEntry(String key, Object value, int type) {
        public SQLEntry(String key, String value) {
            this(key, value, Types.VARCHAR);
        }

        public SQLEntry(String key, Long value) {
            this(key, value, Types.BIGINT);
        }

        public SQLEntry(String key, boolean value) {
            this(key, value, Types.BOOLEAN);
        }

        public void populate(PreparedStatement statement, int index) throws SQLException {
            statement.setObject(index, value, type);
        }
    }
}
