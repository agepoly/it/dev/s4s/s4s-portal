package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.Account;
import ch.students4students.data.AccountWorkshops;
import ch.students4students.data.WorkshopEntry;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Must only be accessed by AccountsRepository or else AccountsRepository's cache might not be up-to-date
 */
public class WorkshopRepository extends SQLRepository<AccountWorkshops> {

    public WorkshopRepository(DataSource source, SecureRandom random, int cacheSize) {
        super(source, random, "workshops", cacheSize);
    }

    @Override
    public void createTableIfAbsent(Statement statement) throws SQLException {
        final String generatedColumns = WorkshopEntry.ALL_WORKSHOPS.stream().map(role -> role.id() + " BOOLEAN NOT NULL").collect(Collectors.joining(", "));
        //noinspection SqlSourceToSinkFlow
        statement.execute("""
                CREATE TABLE IF NOT EXISTS workshops(
                    id VARCHAR NOT NULL,
                    %s,
                    PRIMARY KEY (id)
                )
                """.formatted(generatedColumns));
    }

    @Override
    protected List<Utils.SQLEntry> diff(AccountWorkshops before, AccountWorkshops after) {
        final Set<WorkshopEntry> removedRoles = new HashSet<>(before.workshops());
        removedRoles.removeAll(after.workshops());
        final Set<WorkshopEntry> addedRoles = new HashSet<>(after.workshops());
        addedRoles.removeAll(before.workshops());

        final List<Utils.SQLEntry> diff = new ArrayList<>();
        diff.addAll(removedRoles.stream().map(role -> new Utils.SQLEntry(role.id(), false)).toList());
        diff.addAll(addedRoles.stream().map(role -> new Utils.SQLEntry(role.id(), true)).toList());
        return diff;
    }

    public List<AccountWorkshops> findAllIn(WorkshopEntry workshop) {
        return findAll(new Utils.SQLEntry(workshop.id(), true));
    }

    @Override
    protected AccountWorkshops fromResultSet(Connection connection, ResultSet res) throws SQLException {
        final Set<WorkshopEntry> workshops = new HashSet<>();
        for (WorkshopEntry workshop : WorkshopEntry.ALL_WORKSHOPS) {
            if (res.getBoolean(workshop.id())) {
                workshops.add(workshop);
            }
        }
        return new AccountWorkshops(new AccountId(res.getString("id")), workshops);
    }

    public AccountWorkshops createFor(AccountWorkshops workshops) {
        try (Connection connection = source.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                final String keys = WorkshopEntry.ALL_WORKSHOPS.stream().map(WorkshopEntry::id).collect(Collectors.joining(", "));
                final String values = WorkshopEntry.ALL_WORKSHOPS.stream().map(workshops.workshops()::contains).map(Object::toString).collect(Collectors.joining(", "));

                //noinspection SqlSourceToSinkFlow
                statement.execute("INSERT INTO workshops(id, %s) VALUES ('%s', %s)".formatted(keys, workshops.id().value(), values));
                if (statement.getUpdateCount() != 1) {
                    throw new RuntimeException("Account roles insertion failed");
                }
                cache.put(workshops.id(), workshops);
                return workshops;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Id generateId() {
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void update(AccountWorkshops before, AccountWorkshops after) {
        super.update(before, after);
    }
}
