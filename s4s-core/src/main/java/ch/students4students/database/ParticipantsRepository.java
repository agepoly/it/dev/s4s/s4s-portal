package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.Participant;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ParticipantsRepository extends SQLRepository<Participant> {

    protected ParticipantsRepository(DataSource source, SecureRandom random, int cacheSize) {
        super(source, random, "participants", cacheSize);
    }

    @Override
    protected void createTableIfAbsent(Statement statement) throws SQLException {
        statement.execute("""
                CREATE TABLE IF NOT EXISTS participants(
                   id VARCHAR NOT NULL PRIMARY KEY UNIQUE ,
                   bandColor VARCHAR NOT NULL,
                   welcomeRoom VARCHAR NOT NULL,
                   specialNotes VARCHAR NOT NULL);""");
    }

    @Override
    protected List<Utils.SQLEntry> diff(Participant before, Participant after) {
        throw new RuntimeException("unimplemented");
    }

    @Override
    protected Participant fromResultSet(Connection connection, ResultSet res) throws SQLException {
        return new Participant(new AccountId(res.getString("id")), res.getString("bandColor"), res.getString("welcomeRoom"), res.getString("specialNotes"));
    }

    @Override
    protected Id generateId() {
        throw new RuntimeException("unimplemented");
    }
}
