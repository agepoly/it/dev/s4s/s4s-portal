package ch.students4students.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Clock;

public class S4SDatabase implements AutoCloseable {

    private final HikariDataSource source;
    private final AccountsRepository accountsRepository;
    private final QRCodeRepository qrCodeRepository;
    private final TokensRepository tokensRepository;
    private final RolesRepository rolesRepository;
    private final WorkshopRepository workshopRepository;
    private final ParticipantsRepository participantsRepository;

    public S4SDatabase(String server, String port, String databaseName, String username, String password, int maxConnections, int cacheSize, Clock clock) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://" + server + ":" + port + "/" + databaseName);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setMaximumPoolSize(maxConnections);
        this.source = new HikariDataSource(hikariConfig);
        final SecureRandom random = new SecureRandom();
        this.rolesRepository = new RolesRepository(source, random, cacheSize);
        this.qrCodeRepository = new QRCodeRepository(source, random, cacheSize, clock);
        this.accountsRepository = new AccountsRepository(source, random, cacheSize, rolesRepository);
        this.tokensRepository = new TokensRepository(source, random, cacheSize, clock);
        this.workshopRepository = new WorkshopRepository(source, random, cacheSize);
        this.participantsRepository = new ParticipantsRepository(source, random, cacheSize);
    }

    public void init() {
        try (Connection connection = source.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                rolesRepository.createTableIfAbsent(statement);
                qrCodeRepository.createTableIfAbsent(statement);
                accountsRepository.createTableIfAbsent(statement);
                tokensRepository.createTableIfAbsent(statement);
                workshopRepository.createTableIfAbsent(statement);
                participantsRepository.createTableIfAbsent(statement);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public AccountsRepository accountsRepository() {
        return accountsRepository;
    }

    public QRCodeRepository qrCodeRepository() {
        return qrCodeRepository;
    }

    public TokensRepository tokensRepository() {
        return tokensRepository;
    }

    public RolesRepository rolesRepository() {
        return rolesRepository;
    }

    public ParticipantsRepository participantsRepository() {
        return participantsRepository;
    }

    public WorkshopRepository workshopRepository() {
        return workshopRepository;
    }

    @Override
    public void close() {
        source.close();
    }

    public void flushCache() {
        rolesRepository.flushCache();
        qrCodeRepository.flushCache();
        accountsRepository.flushCache();
        tokensRepository.flushCache();
        workshopRepository.flushCache();
        participantsRepository.flushCache();
    }
}
