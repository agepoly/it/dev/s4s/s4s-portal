package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.AccountRole;
import ch.students4students.data.AccountRoles;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Must only be accessed by AccountsRepository or else AccountsRepository's cache might not be up-to-date
 */
public class RolesRepository extends SQLRepository<AccountRoles> {

    public RolesRepository(DataSource source, SecureRandom random, int cacheSize) {
        super(source, random, "roles", cacheSize);
    }

    @Override
    public void createTableIfAbsent(Statement statement) throws SQLException {
        final String generatedColumns = AccountRole.ALL_ROLES.stream().map(role -> role.id() + " BOOLEAN NOT NULL").collect(Collectors.joining(", "));
        //noinspection SqlSourceToSinkFlow
        statement.execute("""
                CREATE TABLE IF NOT EXISTS roles(
                    id VARCHAR NOT NULL,
                    %s,
                    PRIMARY KEY (id)
                )
                """.formatted(generatedColumns));
    }

    public List<AccountRoles> findAllWithRole(AccountRole accountRole) {
        return findAll(new Utils.SQLEntry(accountRole.id(), true));
    }

    @Override
    protected List<Utils.SQLEntry> diff(AccountRoles before, AccountRoles after) {
        final Set<AccountRole> removedRoles = new HashSet<>(before.roles());
        removedRoles.removeAll(after.roles());
        final Set<AccountRole> addedRoles = new HashSet<>(after.roles());
        addedRoles.removeAll(before.roles());

        final List<Utils.SQLEntry> diff = new ArrayList<>();
        diff.addAll(removedRoles.stream().map(role -> new Utils.SQLEntry(role.id(), false)).toList());
        diff.addAll(addedRoles.stream().map(role -> new Utils.SQLEntry(role.id(), true)).toList());
        return diff;
    }

    @Override
    protected AccountRoles fromResultSet(Connection connection, ResultSet res) throws SQLException {
        final Set<AccountRole> roles = new HashSet<>();
        for (AccountRole role : AccountRole.ALL_ROLES) {
            if (res.getBoolean(role.id())) {
                roles.add(role);
            }
        }
        return new AccountRoles(new AccountId(res.getString("id")), roles);
    }

    public void createFor(AccountRoles account) {
        try (Connection connection = source.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                final String keys = AccountRole.ALL_ROLES.stream().map(AccountRole::id).collect(Collectors.joining(", "));
                final String values = AccountRole.ALL_ROLES.stream().map(account.roles()::contains).map(Object::toString).collect(Collectors.joining(", "));


                //noinspection SqlSourceToSinkFlow
                statement.execute("INSERT INTO roles(id, %s) VALUES ('%s', %s)".formatted(keys, account.id().value(), values));
                if (statement.getUpdateCount() != 1) {
                    throw new RuntimeException("Account roles insertion failed");
                }
                cache.put(account.id(), account);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Id generateId() {
        throw new RuntimeException("unimplemented");
    }
}
