package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.Account;
import ch.students4students.data.AccountRole;
import ch.students4students.data.DiscordUsername;
import ch.students4students.data.EpflMail;
import ch.students4students.data.PersonalMail;
import ch.students4students.data.TelegramUsername;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;
import ch.students4students.data.ids.Key;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class AccountsRepository extends SQLRepository<Account> {

    private final RolesRepository rolesRepository;

    public AccountsRepository(DataSource source, SecureRandom random, int cacheSize, RolesRepository rolesRepository) {
        super(source, random, "accounts", cacheSize);
        this.rolesRepository = rolesRepository;
    }

    public Optional<Account> findByKey(Key key) {
        Optional<Account> inCache = cache.values().stream()
                .filter(account -> account.key().equals(key))
                .findFirst();
        if (inCache.isPresent()) {
            return inCache;
        }
        return find(new Utils.SQLEntry("key", key.value()));
    }

    public Optional<Account> findByPersonalMail(PersonalMail mail) {
        return cache.values().stream()
                .filter(account -> account.personalMail().isPresent())
                .filter(account -> account.personalMail().get().equals(mail))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("personalMail", mail.value())));
    }

    public List<Account> findAllWithRole(AccountRole accountRole) {
        return rolesRepository.findAllWithRole(accountRole).stream().map(roles -> findById(roles.id()).orElseThrow()).toList();
    }

    public Optional<Account> findByEpflMail(EpflMail mail) {
        return cache.values().stream()
                .filter(account -> account.epflMail().isPresent())
                .filter(account -> account.epflMail().get().equals(mail))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("epflMail", mail.value())));
    }

    public Optional<Account> findByTelegramId(Long t) {
        return cache.values().stream()
                .filter(account -> account.telegramId().isPresent())
                .filter(account -> account.telegramId().get().equals(t))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("telegramId", t)));
    }

    public Optional<Account> findByTelegramUsername(TelegramUsername u) {
        return cache.values().stream()
                .filter(account -> account.telegramUsername().isPresent())
                .filter(account -> account.telegramUsername().get().equals(u))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("telegramUsername", u.value())));
    }

    public Optional<Account> findByDiscordId(Long t) {
        return cache.values().stream()
                .filter(account -> account.discordId().isPresent())
                .filter(account -> account.discordId().get().equals(t))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("discordId", t)));
    }

    public Optional<Account> findByDiscordUsername(DiscordUsername u) {
        return cache.values().stream()
                .filter(account -> account.discordUsername().isPresent())
                .filter(account -> account.discordUsername().get().equals(u))
                .findFirst()
                .or(() -> find(new Utils.SQLEntry("discordUsername", u.value())));
    }

    @Override
    public void createTableIfAbsent(Statement statement) throws SQLException {
        statement.execute("""
                 CREATE TABLE IF NOT EXISTS accounts(
                    id VARCHAR NOT NULL PRIMARY KEY UNIQUE ,
                    displayName VARCHAR NOT NULL,
                    firstName VARCHAR NOT NULL,
                    lastName VARCHAR NOT NULL,
                    key VARCHAR,
                    personalMail VARCHAR UNIQUE ,
                    verifiedPersonalMail BOOLEAN NOT NULL,
                    epflMail VARCHAR UNIQUE ,
                    verifiedEpflMail BOOLEAN NOT NULL,
                    telegramId BIGINT UNIQUE,
                    telegramUsername VARCHAR UNIQUE ,
                    verifiedTelegram BOOLEAN NOT NULL,
                    discordId BIGINT UNIQUE ,
                    discordUsername VARCHAR UNIQUE ,
                    verifiedDiscord BOOLEAN NOT NULL);""");
    }

    @Override
    protected List<Utils.SQLEntry> diff(Account before, Account after) {
        final List<Utils.SQLEntry> diff = new ArrayList<>();

        if (!Objects.equals(before.displayName(), after.displayName())) {
            diff.add(new Utils.SQLEntry("displayName", after.displayName()));
        }
        if (!Objects.equals(before.firstName(), after.firstName())) {
            diff.add(new Utils.SQLEntry("firstName", after.firstName()));
        }
        if (!Objects.equals(before.lastName(), after.lastName())) {
            diff.add(new Utils.SQLEntry("lastName", after.lastName()));
        }
        if (!Objects.equals(before.key(), after.key())) {
            diff.add(new Utils.SQLEntry("key", after.key().value()));
        }
        //PersonalMail
        if (!Objects.equals(before.personalMail(), after.personalMail())) {
            diff.add(new Utils.SQLEntry("personalMail", after.personalMail().map(PersonalMail::value).orElse(null)));
        }
        if (before.verifiedPersonalMail() != after.verifiedPersonalMail()) {
            diff.add(new Utils.SQLEntry("verifiedPersonalMail", after.verifiedPersonalMail()));
        }
        //EpflMail
        if (!Objects.equals(before.epflMail(), after.epflMail())) {
            diff.add(new Utils.SQLEntry("epflMail", after.epflMail().map(EpflMail::value).orElse(null)));
        }
        if (before.verifiedEpflMail() != after.verifiedEpflMail()) {
            diff.add(new Utils.SQLEntry("verifiedEpflMail", after.verifiedEpflMail()));
        }
        //Telegram
        if (!Objects.equals(before.telegramId(), after.telegramId())) {
            diff.add(new Utils.SQLEntry("telegramId", after.telegramId().orElse(null)));
        }
        if (!Objects.equals(before.telegramUsername(), after.telegramUsername())) {
            diff.add(new Utils.SQLEntry("telegramUsername", after.telegramUsername().map(TelegramUsername::value).orElse(null)));
        }
        if (before.verifiedTelegram() != after.verifiedTelegram()) {
            diff.add(new Utils.SQLEntry("verifiedTelegram", after.verifiedTelegram()));
        }
        //Discord
        if (!Objects.equals(before.discordId(), after.discordId())) {
            diff.add(new Utils.SQLEntry("discordId", after.discordId().orElse(null)));
        }
        if (!Objects.equals(before.discordUsername(), after.discordUsername())) {
            diff.add(new Utils.SQLEntry("discordUsername", after.discordUsername().map(DiscordUsername::value).orElse(null)));
        }
        if (before.verifiedDiscord() != after.verifiedDiscord()) {
            diff.add(new Utils.SQLEntry("verifiedDiscord", after.verifiedDiscord()));
        }
        return diff;
    }

    @Override
    protected Account fromResultSet(Connection connection, ResultSet res) throws SQLException {
        final AccountId id = new AccountId(res.getString("id"));
        return new Account(
                id,
                res.getString("displayName"),
                res.getString("firstName"),
                res.getString("lastName"),
                new Key(res.getString("key")),
                PersonalMail.create(res.getString("personalMail")),
                res.getBoolean("verifiedPersonalMail"),
                EpflMail.create(res.getString("epflMail")),
                res.getBoolean("verifiedEpflMail"),
                Utils.getOptionalLong(res, "telegramId"),
                TelegramUsername.create(res.getString("telegramUsername")),
                res.getBoolean("verifiedTelegram"),
                Utils.getOptionalLong(res, "discordId"),
                DiscordUsername.create(res.getString("discordUsername")),
                res.getBoolean("verifiedDiscord"),
                rolesRepository.findById(id).orElseThrow()
        );
    }

    @Override
    protected void update(Account before, Account after) {
        super.update(before, after);
        rolesRepository.update(before.roles(), after.roles());
    }

    /**
     * Few conditions :
     * - The account must not already exist in the database
     * - The account must have unique email, EPFL mail, Telegram and Discord or have them null
     * Those constraints will be checked by the database directly due to how the tables are constructed.
     **/
    public void createAccount(Account account) {
        try (Connection connection = source.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("""
                    INSERT INTO accounts(
                            id,
                            displayName,
                            firstName,
                            lastName,
                            key,
                            personalMail,
                            verifiedPersonalMail,
                            epflMail,
                            verifiedEpflMail,
                            telegramId,
                            telegramUsername,
                            verifiedTelegram,
                            discordId,
                            discordUsername,
                            verifiedDiscord
                        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    """)) {

                statement.setString(1, account.id().value());
                statement.setString(2, account.displayName());
                statement.setString(3, account.firstName());
                statement.setString(4, account.lastName());

                statement.setString(5, account.key().value());

                statement.setString(6, account.personalMail().map(PersonalMail::value).orElse(null));
                statement.setBoolean(7, account.verifiedPersonalMail());
                statement.setString(8, account.epflMail().map(EpflMail::value).orElse(null));
                statement.setBoolean(9, account.verifiedEpflMail());
                if (account.telegramId().isEmpty()) {
                    statement.setNull(10, Types.BIGINT);
                } else {
                    statement.setLong(10, account.telegramId().get());
                }
                statement.setString(11, account.telegramUsername().map(TelegramUsername::value).orElse(null));
                statement.setBoolean(12, account.verifiedTelegram());
                if (account.discordId().isEmpty()) {
                    statement.setNull(13, Types.BIGINT);
                } else {
                    statement.setLong(13, account.discordId().get());
                }
                statement.setString(14, account.discordUsername().map(DiscordUsername::value).orElse(null));
                statement.setBoolean(15, account.verifiedDiscord());
                statement.execute();
                if (statement.getUpdateCount() != 1) {
                    throw new RuntimeException("Account insertion failed");
                }
                cache.put(account.id(), account);
                rolesRepository.createFor(account.roles());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Id generateId() {
        return AccountId.generate(random);
    }
}
