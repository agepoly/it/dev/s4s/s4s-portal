package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.QRCode;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;
import ch.students4students.data.ids.QRCodeId;
import ch.students4students.data.ids.QRCodeValue;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class QRCodeRepository extends SQLRepository<QRCode> {

    private final Clock clock;

    public QRCodeRepository(DataSource source, SecureRandom random, int cacheSize, Clock clock) {
        super(source, random, "qrcode", cacheSize);
        this.clock = clock;
    }


    public Optional<QRCode> findByAccountId(AccountId accountId, QRCode.QRCodePurpose purpose) {
        return cache.values().stream()
                .filter(c -> c.accountId().equals(accountId))
                .filter(c -> c.purpose().equals(purpose))
                .findFirst()
                .or(() -> find(
                        new Utils.SQLEntry("accountId", accountId.value()),
                        new Utils.SQLEntry("purpose", purpose.name())));
    }

    @Override
    public void createTableIfAbsent(Statement statement) throws SQLException {
        statement.execute("""
                CREATE TABLE IF NOT EXISTS qrcode(
                    id VARCHAR NOT NULL UNIQUE PRIMARY KEY,
                    accountId VARCHAR NOT NULL,
                    purpose VARCHAR NOT NULL,
                    accessCount BIGINT NOT NULL,
                    lastAccess BIGINT NOT NULL,
                    value VARCHAR NOT NULL UNIQUE
                );
                """);
    }

    public QRCode create(AccountId account, QRCode.QRCodePurpose purpose) {
        final QRCodeId id = generateUniqueId();

        QRCodeValue generated;
        do {
            generated = QRCodeValue.generate(random);
        } while (findByValue(generated).isPresent());

        final QRCode code = new QRCode(id, account, purpose, 0, Instant.EPOCH, generated);

        try (Connection connection = source.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("""
                    INSERT INTO qrcode(
                            id,
                            accountId,
                            purpose,
                            accessCount,
                            lastAccess,
                            value
                    ) VALUES (?, ?, ?, ?, ?, ?)""")) {
                statement.setString(1, id.value());
                statement.setString(2, code.accountId().value());
                statement.setString(3, code.purpose().name());
                statement.setLong(4, code.accessCount());
                statement.setLong(5, code.lastAccess().toEpochMilli());
                statement.setString(6, code.value().value());
                statement.execute();
                if (statement.getUpdateCount() != 1) {
                    throw new RuntimeException("Account insertion failed");
                }
                cache.put(id, code);
                return code;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void markAccess(QRCode code) {
        final QRCode newCode = new QRCode(
                code.id(),
                code.accountId(),
                code.purpose(),
                code.accessCount() + 1,
                clock.instant(),
                code.value());
        update(code, newCode);
    }

    public Optional<QRCode> findByValue(QRCodeValue value) {
        Optional<QRCode> inCache = cache.values().stream()
                .filter(qrCode -> qrCode.value().equals(value))
                .findFirst();
        return inCache.or(() -> find(new Utils.SQLEntry("value", value.value())));
    }

    @Override
    protected List<Utils.SQLEntry> diff(QRCode before, QRCode after) {
        final List<Utils.SQLEntry> diff = new ArrayList<>();

        if (before.accessCount() != after.accessCount()) {
            diff.add(new Utils.SQLEntry("accessCount", after.accessCount()));
        }
        if (!Objects.equals(before.lastAccess(), after.lastAccess())) {
            diff.add(new Utils.SQLEntry("lastAccess", after.lastAccess().toEpochMilli()));
        }
        if (!Objects.equals(before.purpose(), after.purpose())) {
            diff.add(new Utils.SQLEntry("purpose", after.purpose().name()));
        }
        if (!Objects.equals(before.value(), after.value())) {
            diff.add(new Utils.SQLEntry("value", after.value().value()));
        }
        return diff;
    }

    @Override
    protected QRCode fromResultSet(Connection connection, ResultSet res) throws SQLException {
        return new QRCode(
                new QRCodeId(res.getString("id")),
                new AccountId(res.getString("accountId")),
                QRCode.QRCodePurpose.valueOf(res.getString("purpose")),
                res.getLong("accessCount"),
                Instant.ofEpochMilli(res.getLong("lastAccess")),
                new QRCodeValue(res.getString("value")));
    }

    @Override
    protected Id generateId() {
        return QRCodeId.generate(random);
    }
}
