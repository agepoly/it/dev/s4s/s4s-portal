package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.Token;
import ch.students4students.data.TokenType;
import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Id;
import ch.students4students.data.ids.TokenId;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public class TokensRepository extends SQLRepository<Token> {

    private final Clock clock;

    public TokensRepository(DataSource source, SecureRandom random, int cacheSize, Clock clock) {
        super(source, random, "tokens", cacheSize);
        this.clock = clock;
    }

    @Override
    public void createTableIfAbsent(Statement statement) throws SQLException {
        statement.execute("""
                CREATE TABLE IF NOT EXISTS tokens(
                    id VARCHAR NOT NULL UNIQUE PRIMARY KEY ,
                    value VARCHAR NOT NULL,
                    accountId VARCHAR NOT NULL,
                    creation BIGINT NOT NULL,
                    type VARCHAR NOT NULL
                );
                """);
    }

    public boolean verifyCodeAndDelete(AccountId accountId, String code, TokenType type) {
        if (!Token.checkCodeFormat(code)) {
            return false;
        }
        purgeOutdated(clock);
        Optional<Token> token = cache.values().stream()
                .filter(t -> t.type() == type)
                .filter(t -> t.accountId().equals(accountId))
                .filter(t -> t.value().equals(code))
                .findFirst();
        if (token.isEmpty()) {
            token = find(new Utils.SQLEntry("type", type.name()),
                    new Utils.SQLEntry("accountId", accountId.value()),
                    new Utils.SQLEntry("value", code));
        }
        token.ifPresent(this::delete);
        return token.isPresent();
    }

    public Optional<Token> verifySession(String session) {
        if (!Token.checkSessionFormat(session)) {
            return Optional.empty();
        }
        purgeOutdated(clock);
        Optional<Token> token = cache.values().stream()
                .filter(t -> t.type() == TokenType.SESSION)
                .filter(t -> t.value().equals(session))
                .findFirst();
        if (token.isEmpty()) {
            return find(new Utils.SQLEntry("type", TokenType.SESSION.name()),
                    new Utils.SQLEntry("value", session));
        } else {
            return token;
        }
    }

    public Token createToken(AccountId accountId, TokenType type) {
        final TokenId tokenId = generateUniqueId();
        final Token token = Token.create(tokenId, accountId, random, clock, type);

        try (Connection connection = source.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("""
                    INSERT INTO tokens(
                        id,
                        value,
                        accountId,
                        creation,
                        type
                    ) VALUES (?, ?, ?, ?, ?)
                    """)) {
                statement.setString(1, token.id().value());
                statement.setString(2, token.value());
                statement.setString(3, accountId.value());
                statement.setLong(4, token.creation().toEpochMilli());
                statement.setString(5, type.name());

                statement.execute();
                if (statement.getUpdateCount() != 1) {
                    throw new RuntimeException("Account insertion failed");
                }
                cache.put(tokenId, token);
                return token;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void purgeOutdated(Clock clock) {
        try (Connection connection = source.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                for (TokenType type : TokenType.values()) {
                    final long timeLimit = clock.instant().minus(type.validity).toEpochMilli();
                    cache.entrySet().removeIf(e -> e.getValue().type() == type && e.getValue().creation().toEpochMilli() < timeLimit);
                    statement.execute("DELETE FROM tokens WHERE type = '" + type.name() + "' AND creation < " + timeLimit);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected List<Utils.SQLEntry> diff(Token before, Token after) {
        throw new RuntimeException("not implemented");
    }

    @Override
    protected Token fromResultSet(Connection connection, ResultSet res) throws SQLException {
        return new Token(
                new TokenId(res.getString("id")),
                res.getString("value"),
                new AccountId(res.getString("accountId")),
                Instant.ofEpochMilli(res.getLong("creation")),
                TokenType.valueOf(res.getString("type"))
        );
    }

    @Override
    protected Id generateId() {
        return TokenId.generate(random);
    }
}
