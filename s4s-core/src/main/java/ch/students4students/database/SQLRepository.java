package ch.students4students.database;

import ch.students4students.Utils;
import ch.students4students.data.ids.Id;
import ch.students4students.data.ids.Identifiable;

import javax.sql.DataSource;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

abstract class SQLRepository<T extends Identifiable> {

    protected final Map<Id, T> cache;
    protected final DataSource source;
    protected final SecureRandom random;
    private final String tableName;

    protected SQLRepository(DataSource source, SecureRandom random, String tableName, int cacheSize) {
        this.source = source;
        this.random = random;
        this.tableName = tableName;
        this.cache = new LinkedHashMap<>(2 * cacheSize, 1, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Id, T> eldest) {
                return size() > cacheSize;
            }
        };
    }

    protected abstract void createTableIfAbsent(Statement statement) throws SQLException;

        protected void update(T before, T after) {
        if (!before.id().equals(after.id())) {
            throw new IllegalArgumentException("Ids do not match");
        }
        final List<Utils.SQLEntry> query = diff(before, after);
        if (query.isEmpty()) {
            return;
        }
        cache.put(after.id(), after);

        final StringBuilder statementBuild = new StringBuilder("UPDATE " + tableName + " SET ");
        statementBuild.append(query.stream().map(s -> s.key() + " = ? ").collect(Collectors.joining(", ")));
        statementBuild.append("WHERE id = ?;");

        try (Connection connection = source.getConnection()) {
            if (!query.isEmpty()) {
                try (PreparedStatement statement = connection.prepareStatement(statementBuild.toString())) {
                    for (int i = 0; i < query.size(); i++) {
                        query.get(i).populate(statement, i + 1);
                    }
                    statement.setString(query.size() + 1, after.id().value());
                    statement.execute();
                    if (statement.getUpdateCount() != 1) {
                        throw new RuntimeException("Update failed");
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void delete(T t) {
        delete(t.id());
    }

    protected void delete(Id id) {
        cache.remove(id);
        try (Connection connection = source.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM " + tableName + " WHERE id = ?;")) {
                statement.setString(1, id.value());
                statement.execute();
                if (statement.getUpdateCount() < 1) {
                    throw new RuntimeException("Deletion failed. Id: " + id);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract List<Utils.SQLEntry> diff(T before, T after);

    public Optional<T> findById(Id id) {
        T t = cache.get(id);
        if (t == null) {
            return find(new Utils.SQLEntry("id", id.value()));
        } else {
            return Optional.of(t);
        }
    }

    /**
     * This method doesn't search in the cache before querying the database.
     */
    protected Optional<T> find(Utils.SQLEntry... entries) {
        return find(true, entries).stream().findFirst();
    }

    private List<T> find(boolean first, Utils.SQLEntry... entries) {
        try (Connection connection = source.getConnection()) {
            String condition = Arrays.stream(entries).map(e -> e.key() + " = ?").collect(Collectors.joining(" AND "));
            String query = "SELECT * FROM " + tableName + " WHERE " + condition + (first ? " LIMIT 1;" : ";");
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                int i = 0;
                for (Utils.SQLEntry entry : entries) {
                    entry.populate(statement, ++i);
                }
                statement.execute();

                ResultSet res = statement.getResultSet();
                final List<T> list = new ArrayList<>();
                while (res.next()) {
                    final T t = fromResultSet(connection, res);
                    cache.put(t.id(), t);
                    list.add(t);
                }
                return list;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> findAll(Utils.SQLEntry... entries) {
        return find(false, entries);
    }

    protected abstract T fromResultSet(Connection connection, ResultSet res) throws SQLException;

    public void flushCache() {
        cache.clear();
    }

    public <P extends Id> P generateUniqueId() {
        @SuppressWarnings("unchecked") final P id = (P) generateId();
        return findById(id).isPresent() ? generateUniqueId() : id;
    }

    protected abstract Id generateId();
}
