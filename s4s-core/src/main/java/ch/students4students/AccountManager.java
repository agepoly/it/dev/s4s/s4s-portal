package ch.students4students;

import ch.students4students.data.Account;
import ch.students4students.data.DiscordUsername;
import ch.students4students.data.PersonalMail;
import ch.students4students.data.TelegramUsername;
import ch.students4students.data.Token;
import ch.students4students.data.TokenType;
import ch.students4students.data.ids.Key;
import ch.students4students.database.AccountsRepository;
import ch.students4students.database.TokensRepository;

import java.util.Optional;

public class AccountManager {

    private final AccountsRepository accounts;
    private final TokensRepository tokens;
    private final DiscordBot discordBot;
    private final TelegramBot telegramBot;

    public AccountManager(AccountsRepository accounts, TokensRepository tokens, DiscordBot discordBot, TelegramBot telegramBot) {
        this.accounts = accounts;
        this.tokens = tokens;
        this.discordBot = discordBot;
        this.telegramBot = telegramBot;
    }

    public boolean sendDiscordCode(DiscordUsername username) {
        Optional<Account> account = accounts.findByDiscordUsername(username);
        if (account.isEmpty() || !account.get().verifiedDiscord()) {
            return false;
        }
        Token token = tokens.createToken(account.get().id(), TokenType.DISCORD_CODE);
        discordBot.sendCode(account.get().discordId().orElseThrow(), token.value());
        return true;
    }

    public Optional<Token> discordLogin(DiscordUsername username, String code) {
        Optional<Account> account = accounts.findByDiscordUsername(username);
        if (account.isEmpty() || !account.get().verifiedDiscord()) {
            return Optional.empty();
        }
        if (!tokens.verifyCodeAndDelete(account.get().id(), code, TokenType.DISCORD_CODE)) {
            return Optional.empty();
        }
        return Optional.of(tokens.createToken(account.get().id(), TokenType.SESSION));
    }

    public Optional<Token> isAuthenticated(String loginToken) {
        return tokens.verifySession(loginToken);
    }

    public boolean sendTelegramCode(TelegramUsername username) {
        Optional<Account> account = accounts.findByTelegramUsername(username);
        if (account.isEmpty() || !account.get().verifiedTelegram()) {
            return false;
        }
        Token token = tokens.createToken(account.get().id(), TokenType.TELEGRAM_CODE);
        telegramBot.sendCode(account.get().telegramId().orElseThrow(), token.value());
        return true;
    }

    public Optional<Token> telegramLogin(TelegramUsername username, String code) {
        Optional<Account> account = accounts.findByTelegramUsername(username);
        if (account.isEmpty() || !account.get().verifiedTelegram()) {
            return Optional.empty();
        }
        if (!tokens.verifyCodeAndDelete(account.get().id(), code, TokenType.TELEGRAM_CODE)) {
            return Optional.empty();
        }
        return Optional.of(tokens.createToken(account.get().id(), TokenType.SESSION));
    }

    public boolean sendMailCode(PersonalMail personalMail) {
        //TODO implement
        return false;
    }

    public Optional<Token> mailLogin(PersonalMail personalMail, String code) {
        //TODO implement
        return Optional.empty();
    }

    public Optional<Token> keyLogin(Key key) {
        return accounts.findByKey(key).map(value -> tokens.createToken(value.id(), TokenType.SESSION));
    }
}
