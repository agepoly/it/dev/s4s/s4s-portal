package ch.students4students.data;

import java.util.Arrays;
import java.util.Optional;

public enum Section {
    AR("AR", "Architecture"),
    CGC("CGC", "Chimie et génie chimique"),
    GC("GC", "Génie civil"),
    SC("SC", "Systèmes de communication"),
    IN("IN", "Informatique"),
    EL("EL", "Génie électrique et électronique"),
    SIE("SIE", "Sciences et ingénierie de l'environnement"),
    SV("SV", "Sciences de la vie"),
    MX("MX", "Science et génie des matériaux"),
    MA("MA", "Mathématiques"),
    GM("GM", "Génie mécanique"),
    MT("MT", "Microtechnique"),
    PH("PH", "Physique"),
    CMS("CMS", "Cours de mathématiques spéciales");

    private final String shortName;
    private final String frenchName;

    Section(String shortName, String frenchName) {
        this.shortName = shortName;
        this.frenchName = frenchName;
    }

    public static Optional<Section> fromFrenchName(String frenchName) {
        return Arrays.stream(values()).filter(section -> section.frenchName.equals(frenchName)).findFirst();
    }

    public static Optional<Section> fromShortName(String shortName) {
        return Arrays.stream(values()).filter(section -> section.shortName.equals(shortName)).findFirst();
    }

    public String shortName() {
        return shortName;
    }
    public String frenchName() {
        return frenchName;
    }
}
