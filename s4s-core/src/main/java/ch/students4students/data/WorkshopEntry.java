package ch.students4students.data;


import java.util.Set;

public record WorkshopEntry(String id) {
    public static final Set<WorkshopEntry> ALL_WORKSHOPS = Set.of(
            new WorkshopEntry("ws_PolympiadesMath"),
            new WorkshopEntry("ws_PolympiadesProg"),
            new WorkshopEntry("ws_iGem"),
            new WorkshopEntry("ws_CLIC"),
            new WorkshopEntry("ws_CLICGAME"),
            new WorkshopEntry("ws_XploreFormationMecanique"),
            new WorkshopEntry("ws_XploreFormationElectronique"),
            new WorkshopEntry("ws_XploreLesLimitesdeletreHumain"),
            new WorkshopEntry("ws_ApprendreaetudierRolex"),
            new WorkshopEntry("ws_ThomasKVendredi30"),
            new WorkshopEntry("ws_ThomasKJeudi5"),
            new WorkshopEntry("ws_SPOTAtrium14ha16h"),
            new WorkshopEntry("ws_SPOTAtrium16ha18h"),
            new WorkshopEntry("ws_ZEG"),
            new WorkshopEntry("ws_Tania"),
            new WorkshopEntry("ws_Arcanite"),
            new WorkshopEntry("ws_BioSense"),
            new WorkshopEntry("ws_JDRleJeudi29"),
            new WorkshopEntry("ws_JDRleMardi3"),
            new WorkshopEntry("ws_JDRleMardi3Mail"),
            new WorkshopEntry("ws_unused1"),
            new WorkshopEntry("ws_unused2"),
            new WorkshopEntry("ws_unused3"),
            new WorkshopEntry("ws_unused4"),
            new WorkshopEntry("ws_unused5"),
            new WorkshopEntry("ws_unused6"),
            new WorkshopEntry("ws_unused7"),
            new WorkshopEntry("ws_unused8"),
            new WorkshopEntry("ws_unused9"),
            new WorkshopEntry("ws_unused10")
    );

    public WorkshopEntry {
        if (id.matches("^ws_[a-zA-Z0-9_]*]$")) {
            throw new IllegalArgumentException("Invalid workshop : " + id);
        }
    }

    public static int max(WorkshopEntry entry) {
        return switch (entry.id) {
            case "ws_SPOTAtrium14ha16h" -> 75;
            case "ws_SPOTAtrium16ha18h" -> 75;
            case "ws_PolympiadesMath" -> 60;
            case "ws_PolympiadesProg" -> 35;
            case "ws_iGem" -> 20;
            case "ws_CLIC" -> 0;//STOP registrations for CLIC
            case "ws_ApprendreaetudierRolex" -> 300;
            default -> Integer.MAX_VALUE;
        };
    }
}