package ch.students4students.data;

import java.util.HashSet;
import java.util.Set;

public record AccountRole(String id) {

    public static final AccountRole ADMIN = new AccountRole("rol_admin");
    public static final AccountRole BOARD_2024 = new AccountRole("rol_board_2024");
    public static final AccountRole STAFF_2024 = new AccountRole("rol_staff_2024");
    public static final AccountRole RC_2024 = new AccountRole("rol_rc_2024");
    public static final AccountRole WEEK_PARTICIPANT_2024 = new AccountRole("rol_week_participant_2024");
    public static final Set<AccountRole> ALL_ROLES = allRoles();

    public AccountRole {
        if (id.matches("^rol_[a-zA-Z0-9_]*]$")) {
            throw new IllegalArgumentException("Invalid role : " + id);
        }
    }

    private static Set<AccountRole> allRoles() {
        Set<AccountRole> roles = new HashSet<>();
        roles.add(ADMIN);
        roles.add(BOARD_2024);
        roles.add(STAFF_2024);
        roles.add(RC_2024);
        roles.add(WEEK_PARTICIPANT_2024);
        roles.addAll(Course.ALL_COURSES_ROLES);
        return roles;
    }
}
