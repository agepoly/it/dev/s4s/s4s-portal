package ch.students4students.data;

import java.util.Arrays;
import java.util.Optional;

public enum Semester {

    BA1("BA1"),
    BA2("BA2"),
    BA3("BA3"),
    BA4("BA4"),
    BA5("BA5"),
    BA6("BA6"),
    MA1("MA1"),
    MA2("MA2"),
    MA3("MA3"),
    MA4("MA4"),
    Other("Autre");

    private final String shortName;

    Semester(String shortName) {
        this.shortName = shortName;
    }

    public static Optional<Semester> fromShortName(String shortName) {
        return Arrays.stream(values()).filter(semester -> semester.shortName.equals(shortName)).findFirst();
    }

    public String shortName() {
        return shortName;
    }
}
