package ch.students4students.data;

import java.util.Arrays;
import java.util.Optional;

public enum DoublingStatus {
    FIRST_TRY("C'est ma première année à l'EPFL"),
    CMS("J'ai déjà fait le CMS"),
    DOUBLING("C'est ma deuxième tentative");

    private final String frenchText;

    DoublingStatus(String frenchText) {
        this.frenchText = frenchText;
    }

    public static Optional<DoublingStatus> fromFrenchText(String frenchText) {
        return Arrays.stream(values()).filter(section -> section.frenchText.equals(frenchText)).findFirst();
    }

    public String getFrenchText() {
        return frenchText;
    }
}
