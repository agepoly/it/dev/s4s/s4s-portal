package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.CandidatureId;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

public record Candidature(
        CandidatureId id,
        Instant createdAt,
        AccountId accountId,
        Section section,
        Semester semester,
        String description,
        Map<Course, Set<CourseRole>> roles) {
}