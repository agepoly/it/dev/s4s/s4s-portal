package ch.students4students.data;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.SECONDS;

public enum TokenType {
    DISCORD_CODE(Duration.ofMinutes(5)),
    TELEGRAM_CODE(Duration.ofMinutes(5)),
    SESSION(Duration.ofDays(69));

    public final Duration validity;

    TokenType(Duration validity) {
        this.validity = validity;
    }
}
