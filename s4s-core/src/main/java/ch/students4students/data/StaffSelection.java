package ch.students4students.data;

import ch.students4students.data.ids.CandidatureId;
import ch.students4students.data.ids.StaffSelectionId;

import java.util.Map;

public record StaffSelection(
        StaffSelectionId id,
        CandidatureId candidatureId,
        Course course,
        Map<CourseRole, Boolean> roles,
        boolean confirmed,
        String notes) {
}
