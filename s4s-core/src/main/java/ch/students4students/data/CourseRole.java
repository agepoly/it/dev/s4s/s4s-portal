package ch.students4students.data;

public enum CourseRole {
    WRITER, SPEAKER, ASSISTANT
}
