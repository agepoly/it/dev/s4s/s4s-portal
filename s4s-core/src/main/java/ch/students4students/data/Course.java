package ch.students4students.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public enum Course {
    ANALYSIS("co-ana", "Analyse"),
    ADVANCED_ANALYSIS("co-anaav", "Analyse avancée"),
    LINEAR_ALGEBRA("co-alin", "Algèbre linéaire"),
    ADVANCED_LINEAR_ALGEBRA("co-alinav", "Algèbre linéaire avancée"),
    ELECTRICITY("co-el", "Electricité"),
    MECHANICAL_PHYSICS("co-phy", "Physique mécanique"),
    PROGRAMMING("co-prog", "Programmation"),
    AICC("co-aicc", "AICC"),
    CHEMISTRY("co-chi", "Chimie"),
    ANALYSIS_CMS("co-anacms", "Analyse (CMS)"),
    ALGEBRA_CMS("co-alcms", "Algèbre (CMS)"),
    ANALYTICAL_GEOMETRY_CMS("co-geom", "Géométrie analytique (CMS)"),
    GENERAL_PHYSICS_CMS("co-phycms", "Physique générale (CMS)");

    public static final Set<AccountRole> ALL_COURSES_ROLES = allCoursesRoles();
    /**
     * The unique identifier of the course, used in the database
     * for naming related tables and to name the corresponding roles.
     */
    private final String id;
    private final String frenchName;
    private final AccountRole rcRole;
    private final AccountRole staffRole;

    Course(String id, String frenchName) {
        if (!id.startsWith("co-")) {
            throw new IllegalArgumentException("Invalid course id");
        }
        this.id = id;
        this.frenchName = frenchName;
        this.rcRole = new AccountRole("rol_" + id.replace('-', '_') + "_rc");
        this.staffRole = new AccountRole("rol_" + id.replace('-', '_') + "_staff");
    }

    public static Optional<Course> fromFrenchName(String frenchName) {
        return Arrays.stream(values()).filter(course -> course.frenchName.equals(frenchName)).findFirst();
    }

    private static Set<AccountRole> allCoursesRoles() {
        final Set<AccountRole> result = new HashSet<>();
        for (Course course : values()) {
            result.add(course.rcRole());
            result.add(course.staffRole());
        }
        return result;
    }

    public String id() {
        return id;
    }

    public String frenchName() {
        return frenchName;
    }

    public AccountRole rcRole() {
        return rcRole;
    }

    public AccountRole staffRole() {
        return staffRole;
    }
}
