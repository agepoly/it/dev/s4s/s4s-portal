package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Identifiable;
import ch.students4students.data.ids.Key;

import java.util.Optional;
import java.util.Set;

public record Account(
        AccountId id,
        String displayName,
        String firstName,
        String lastName,
        Key key,
        Optional<PersonalMail> personalMail,
        boolean verifiedPersonalMail,
        Optional<EpflMail> epflMail,
        boolean verifiedEpflMail,
        Optional<Long> telegramId,
        Optional<TelegramUsername> telegramUsername,
        boolean verifiedTelegram,
        Optional<Long> discordId,
        Optional<DiscordUsername> discordUsername,
        boolean verifiedDiscord,
        AccountRoles roles) implements Identifiable {
}
