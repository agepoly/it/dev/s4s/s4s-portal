package ch.students4students.data;

import java.util.Optional;
import java.util.regex.Pattern;

public record PersonalMail(String value) {

    private static final Pattern pattern = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

    public PersonalMail {
        if (!isValid(value)) {
            throw new IllegalArgumentException("Mail is not valid");
        }
    }

    public static Optional<PersonalMail> create(String value) {
        if (isValid(value)) {
            return Optional.of(new PersonalMail(value));
        } else {
            return Optional.empty();
        }
    }

    public static boolean isValid(String value) {
        return value != null && pattern.matcher(value).matches();
    }
}
