package ch.students4students.data;


import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Conditions to be a valid Discord username come from
 * <a href="https://support.discord.com/hc/en-us/articles/12620128861463-New-Usernames-Display-Names#h_01GXPQAGG6W477HSC5SR053QG1">
 * this official Discord blog post
 * </a>.
 */
public record DiscordUsername(String value) {

    private static final Pattern pattern = Pattern.compile("^[a-zA-Z0-9_.]{2,32}$");

    public DiscordUsername {
        if (!isValid(value)) {
            throw new IllegalArgumentException("Invalid Telegram username: " + value);
        }
    }

    public static Optional<DiscordUsername> create(String value) {
        if (isValid(value)) {
            return Optional.of(new DiscordUsername(value));
        } else {
            return Optional.empty();
        }
    }

    public static boolean isValid(String value) {
        return value != null && (value.length() >= 2 && value.length() <= 32) && pattern.matcher(value).matches();
    }
}