package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Identifiable;
import ch.students4students.data.ids.QRCodeId;
import ch.students4students.data.ids.QRCodeValue;

import java.time.Instant;

public record QRCode(QRCodeId id,
                     AccountId accountId,
                     QRCodePurpose purpose,
                     long accessCount,
                     Instant lastAccess,
                     QRCodeValue value) implements Identifiable {

    public enum QRCodePurpose {
        S4S_2024_WEEK_PARTICIPANT
    }
}
