package ch.students4students.data.ids;

import java.util.regex.Pattern;

public record Key(String value) {

    private static final Pattern pattern = Pattern.compile("^s4skey_[-0-9]*$");

    public Key {
        if (!isValid(value)) {
            throw new IllegalArgumentException("Key is not valid");
        }
    }

    public static boolean isValid(String value) {
        return value != null && pattern.matcher(value).matches();
    }
}
