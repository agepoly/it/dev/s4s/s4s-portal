package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record TokenId(String value) implements Id {
    private static final Pattern pattern = Pattern.compile("^tokid-[0-9]*$");

    public TokenId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static TokenId generate(Random random) {
        return new TokenId("tokid-" + random.nextLong(Long.MAX_VALUE));
    }
}
