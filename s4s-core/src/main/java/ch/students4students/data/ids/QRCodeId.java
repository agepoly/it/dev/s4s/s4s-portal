package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record QRCodeId(String value) implements Id {
    private static final Pattern pattern = Pattern.compile("^qrid-[0-9]*$");

    public QRCodeId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static QRCodeId generate(Random random) {
        return new QRCodeId("qrid-" + random.nextLong(Long.MAX_VALUE));
    }
}
