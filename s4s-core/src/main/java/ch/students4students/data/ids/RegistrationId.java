package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record RegistrationId(String value) {
    private static final Pattern pattern = Pattern.compile("^ri-[0-9]*$");

    public RegistrationId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static RegistrationId generate(Random random) {
        return new RegistrationId("ri-" + random.nextLong(Long.MAX_VALUE));
    }
}
