package ch.students4students.data.ids;

@FunctionalInterface
public interface Id {
    String value();
}
