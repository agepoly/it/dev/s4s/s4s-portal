package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record StaffSelectionId(String value) {
    private static final Pattern pattern = Pattern.compile("^ssi-[0-9]*$");

    public StaffSelectionId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static StaffSelectionId generate(Random random) {
        return new StaffSelectionId("ssi-" + random.nextLong(Long.MAX_VALUE));
    }
}
