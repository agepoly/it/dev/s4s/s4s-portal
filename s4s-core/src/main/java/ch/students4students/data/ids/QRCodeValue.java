package ch.students4students.data.ids;

import java.security.SecureRandom;
import java.util.regex.Pattern;

public record QRCodeValue(String value) implements Id {
    private static final Pattern pattern = Pattern.compile("^qr-[0-9]*$");

    public QRCodeValue {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static boolean isValid(String value) {
        return pattern.matcher(value).matches();
    }

    public static QRCodeValue generate(SecureRandom random) {
        return new QRCodeValue("qr-" + random.nextLong(Long.MAX_VALUE) + random.nextLong(Long.MAX_VALUE));
    }
}
