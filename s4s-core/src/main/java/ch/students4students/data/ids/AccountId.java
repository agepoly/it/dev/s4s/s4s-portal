package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record AccountId(String value) implements Id {
    private static final Pattern pattern = Pattern.compile("^acc-[0-9]*$");

    public AccountId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static AccountId generate(Random random) {
        return new AccountId("acc-" + random.nextLong(Long.MAX_VALUE));
    }
}
