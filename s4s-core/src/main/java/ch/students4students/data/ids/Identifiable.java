package ch.students4students.data.ids;

@FunctionalInterface
public interface Identifiable {
    Id id();
}
