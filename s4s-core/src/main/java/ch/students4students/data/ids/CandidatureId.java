package ch.students4students.data.ids;

import java.util.Random;
import java.util.regex.Pattern;

public record CandidatureId(String value) {
    private static final Pattern pattern = Pattern.compile("^ci-[0-9]*$");

    public CandidatureId {
        if (!pattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid id : " + value);
        }
    }

    public static CandidatureId generate(Random random) {
        return new CandidatureId("ci-" + random.nextLong(Long.MAX_VALUE));
    }
}
