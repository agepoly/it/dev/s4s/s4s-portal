package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.RegistrationId;

import java.time.Instant;

/**
 * Represents the registration of a future student that wants to
 * be a participant in the Students 4 Students week (not as a
 * staff, really as a participant).
 *
 * @param id             Each registration has a unique identifier.
 * @param accountId      The account of the corresponding student.
 * @param section        The section the student is in.
 * @param doublingStatus Whether the student doubled or already did the CMS.
 * @param special        Whether the student has any special needs.
 * @param remarks        Additional precisions about the special needs if any. Can be null if no special needs.
 */
public record Registration(
        RegistrationId id,
        Instant createdAt,
        AccountId accountId,
        Section section,
        DoublingStatus doublingStatus,
        boolean special,
        String remarks) {
}
