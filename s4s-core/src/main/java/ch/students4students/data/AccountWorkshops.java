package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Identifiable;

import java.util.Set;

public record AccountWorkshops(AccountId id, Set<WorkshopEntry> workshops) implements Identifiable {
}
