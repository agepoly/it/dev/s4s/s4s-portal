package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Identifiable;
import ch.students4students.data.ids.TokenId;

import java.security.SecureRandom;
import java.time.Clock;
import java.time.Instant;
import java.util.regex.Pattern;

public record Token(
        TokenId id,
        String value,
        AccountId accountId,
        Instant creation,
        TokenType type) implements Identifiable {

    private static final Pattern sessionPattern = Pattern.compile("^tok-[0-9]*$");
    private static final Pattern codePattern = Pattern.compile("^[0-9]{9}$");

    public Token {
        if (!sessionPattern.matcher(value).matches() && !codePattern.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid token value : " + value);
        }
    }

    public static boolean checkSessionFormat(String session) {
        return sessionPattern.matcher(session).matches();
    }

    public static boolean checkCodeFormat(String code) {
        return codePattern.matcher(code).matches();
    }

    public static Token create(TokenId id, AccountId accountId, SecureRandom random, Clock clock, TokenType type) {
        final String value = type == TokenType.SESSION
                ? ("tok-" + random.nextLong(Long.MAX_VALUE) + random.nextLong(Long.MAX_VALUE))
                : Integer.toString(random.nextInt(900_000_000) + 100_000_000);
        return new Token(id, value, accountId, clock.instant(), type);
    }
}
