package ch.students4students.data;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * A Telegram username can contain only alphanumeric characters along with
 * underscores. It must be between 5 and 32 characters long (inclusive)
 * and must start with a letter (a digit or an underscore are not allowed
 * as the first character).
 * <p>
 * Note : Telegram usernames are case-insensitive, but case preferences
 * are preserved. It does not matter whether SomeUsername is used instead
 * of someusername or SoMeUsErNaMe.
 */
public record TelegramUsername(String value) {

    private static final Pattern pattern = Pattern.compile("^@[a-zA-Z][a-zA-Z0-9_]{4,31}$");

    public TelegramUsername {
        if (!isValid(value)) {
            throw new IllegalArgumentException("Invalid Telegram username: " + value);
        }
    }

    public static Optional<TelegramUsername> create(String value) {
        if (isValid(value)) {
            return Optional.of(new TelegramUsername(value));
        } else {
            return Optional.empty();
        }
    }

    public static boolean isValid(String value) {
        return value != null && (value.length() >= 5 && value.length() <= 32) && pattern.matcher(value).matches();
    }
}