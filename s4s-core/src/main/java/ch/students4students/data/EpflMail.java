package ch.students4students.data;

import java.util.Optional;
import java.util.regex.Pattern;

public record EpflMail(String value) {

    public EpflMail {
        if (!isValid(value)) {
            throw new IllegalArgumentException("Mail is not valid");
        }
    }

    public static Optional<EpflMail> create(String value) {
        if (isValid(value)) {
            return Optional.of(new EpflMail(value));
        } else {
            return Optional.empty();
        }
    }

    public static boolean isValid(PersonalMail personalMail) {
        return personalMail.value().endsWith("@epfl.ch");
    }

    public static boolean isValid(String value) {
        return PersonalMail.isValid(value) && value.endsWith("@epfl.ch");
    }
}
