package ch.students4students.data;

import ch.students4students.data.ids.AccountId;
import ch.students4students.data.ids.Identifiable;

public record Participant(AccountId id, String bandColor, String welcomeRoom,
                          String specialNotes) implements Identifiable {
}
