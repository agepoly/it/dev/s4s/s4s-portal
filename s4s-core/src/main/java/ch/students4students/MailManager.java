package ch.students4students;

import ch.students4students.data.PersonalMail;
import ch.students4students.data.ids.Key;
import jakarta.mail.Address;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MailManager implements AutoCloseable {
    private static final Logger log = Logger.getLogger(MailManager.class.getName());
    private final int cooldownPeriodMillis;
    private final Session session;
    private final PersonalMail s4sMail;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private int numberOfMessages = 0;

    public MailManager(String host, String port, PersonalMail mail, String password, int cooldownPeriodMillis) {
        this.s4sMail = mail;

        this.cooldownPeriodMillis = cooldownPeriodMillis;
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);
        this.session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mail.value(), password);
            }
        });
        log.info("Mail session created with username " + mail);
    }

    public int averageTime() {
        return numberOfMessages * cooldownPeriodMillis;
    }

    public void sendLostKey(PersonalMail to, Key key) throws MessagingException {
        final MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent("""
                Bonjour,
                                
                Voici votre clé S4S : %s
                                
                Vous pouvez l'utiliser pour vous connecter à votre compte.
                                
                Ceci est un message automatique, merci de ne pas y répondre. Pour toute question, veuillez contacter l'adresse s4s@epfl.ch.
                                
                Cordialement,
                                
                L'équipe S4S
                """.formatted(key.value()), "text/plain; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        scheduleMessage(to, "Récupération de la clé S4S", multipart);
    }

    private void scheduleMessage(PersonalMail to, String subject, Multipart multipart) throws MessagingException {
        final Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(s4sMail.value()));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse(to.value()));
        message.setSubject(subject);
        message.setContent(multipart);
        log.info("Schedule mail '" + subject + "' to '" + to.value() + "'");
        numberOfMessages++;
        executor.submit(new SendMessageTask(message));
    }

    @Override
    public void close() {
        log.info("Waiting for executor termination. 1 min max.");
        try {
            if (executor.awaitTermination(1, TimeUnit.MINUTES)) {
                log.info("Executor termination has succeeded");
            } else {
                log.severe("Executor termination has failed");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            final List<Runnable> unfinished = executor.shutdownNow();
            log.severe("Executor has terminated with uncompleted tasks:\n" + unfinished.stream().map(runnable -> (SendMessageTask) runnable).map(SendMessageTask::toString).collect(Collectors.joining("\n")));
        }
    }

    private class SendMessageTask implements Runnable {
        private final Message message;

        private SendMessageTask(Message message) {
            this.message = message;
        }

        @Override
        public void run() {
            try {
                Transport.send(message);
                log.info("Sent " + this);
            } catch (MessagingException e) {
                log.severe("Error sending " + this);
            }
            try {
                Thread.sleep(cooldownPeriodMillis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            numberOfMessages--;
        }

        @Override
        public String toString() {
            try {
                return "mail'" + message.getSubject() + "' to '" + Arrays.stream(message.getAllRecipients()).map(Address::toString).collect(Collectors.joining("; ")) + "'";
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
