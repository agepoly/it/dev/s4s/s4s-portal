/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.path.Matching;
import ch.kalajdzic.services.rest.path.PartedPath;

import java.util.ArrayList;
import java.util.List;

public class Container {

    private final List<AnnotatedMethod> methods = new ArrayList<>();

    public void add(AnnotatedMethod annotatedMethod) {
        methods.add(annotatedMethod);
    }

    AnnotatedMethod findBestMethod(PartedPath requestPath) {
        final BestValueRetriever<AnnotatedMethod> bestValueRetriever = new BestValueRetriever<>(new Method404());

        for (AnnotatedMethod annotatedMethod : methods) {
            final Matching matching = annotatedMethod.match(requestPath);
            if (matching.perfectlyMatch()) {
                return annotatedMethod;
            }
            if (!matching.dontMatch()) {
                bestValueRetriever.compareRival(annotatedMethod, matching.score());
            }
        }
        return bestValueRetriever.retrieve();
    }

    private static class BestValueRetriever<T> {
        private T value;
        private int valueScore;

        private BestValueRetriever(T defaultValue) {
            this.value = defaultValue;
            this.valueScore = Integer.MIN_VALUE;
        }

        void compareRival(T rival, int rivalScore) {
            if (rivalScore > valueScore) {
                value = rival;
                valueScore = rivalScore;
            }
        }

        T retrieve() {
            return value;
        }
    }
}
