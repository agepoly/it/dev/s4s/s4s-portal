/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest.path;

import ch.kalajdzic.services.rest.annotations.Path;

import static java.util.Objects.isNull;

public class PartedPath {

    private final String path;
    private final String[] parts;

    public PartedPath(String path) {
        this.path = isNull(path) ? "/" : path;
        this.parts = this.path.split("/");
    }

    public PartedPath(Path path) {
        this(isNull(path) ? null : path.value());
    }

    private PartedPath(String parent, String path) {
        this((parent + path).replace("//", "/"));
    }

    public PartedPath(PartedPath parent, Path path) {
        this(isNull(parent) ? "/" : parent.path, isNull(path) ? "/" : path.value());
    }

    public boolean isEmpty() {
        return parts.length == 0;
    }

    public String path() {
        return path;
    }

    public String part(int i) {
        return parts[i];
    }

    public boolean isArgumentPart(int i) {
        return parts[i].startsWith("$");
    }

    public int partCount() {
        return parts.length;
    }
}
