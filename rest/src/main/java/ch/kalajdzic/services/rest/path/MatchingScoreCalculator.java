/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest.path;

import java.util.function.BiFunction;
import java.util.function.Function;

public class MatchingScoreCalculator {
    public Matching calculate(PartedPath path, PartedPath requestPath) {
        if (path.path().equals(requestPath.path())) {
            return Matching.PERFECTLY_MATCH;
        }
        return forEachParts(path, requestPath, (pathPart, requestPathPart) -> {
            if (pathPart.equals(requestPathPart)) {
                return Matching.PERFECTLY_MATCH;
            } else if (pathPart.startsWith("$")) {
                return new Matching(0);
            }
            return Matching.DONT_MATCH;
        });
    }

    private Matching forEachParts(PartedPath path, PartedPath requestPath, BiFunction<String, String, Matching> consumer) {
        if (path.partCount() != requestPath.partCount()) {
            return Matching.DONT_MATCH;
        }
        return specialSum(path.partCount(), i -> consumer.apply(path.part(i), requestPath.part(i)));
    }

    private Matching specialSum(int count, Function<Integer, Matching> consumer) {
        int sum = 0;
        for (int i = 0; i < count; i++) {
            final Matching partMatching = consumer.apply(i);
            if (partMatching.perfectlyMatch()) {
                sum += Math.pow(2, count - i);
            } else if (partMatching.dontMatch()) {
                return Matching.DONT_MATCH;
            }
        }
        return new Matching(sum);
    }
}
