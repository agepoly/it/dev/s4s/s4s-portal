/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.annotations.Child;
import ch.kalajdzic.services.rest.annotations.Path;
import ch.kalajdzic.services.rest.path.PartedPath;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

class AnnotationParser<T> {

    private final Map<HttpMethods, Container> containers = new HashMap<>();
    private final T service;

    AnnotationParser(T service) {
        this.service = service;
        for (HttpMethods method : HttpMethods.values()) {
            containers.put(method, new Container());
        }
    }

    Map<HttpMethods, Container> parseService() throws ReflectiveOperationException {
        parseService(service, null);
        return containers;
    }

    private void parseService(Object service, PartedPath parent) throws ReflectiveOperationException {
        final PartedPath path = new PartedPath(parent, service.getClass().getAnnotation(Path.class));

        for (Method method : service.getClass().getMethods()) {
            parseMethod(method, path, service);
        }
    }

    private void parseMethod(Method method, PartedPath servicePath, Object service) throws ReflectiveOperationException {
        if (method.isAnnotationPresent(Child.class)) {
            final PartedPath path = new PartedPath(servicePath, method.getAnnotation(Path.class));
            parseService(method.invoke(service), path);
            return;
        }
        HttpMethods.fromAnnotations(method).map(containers::get).forEach(container -> {
            final PartedPath path = new PartedPath(servicePath, method.getAnnotation(Path.class));
            final String contentType = ContentTypes.from(method);
            container.add(new AnnotatedMethod(service, method, path, contentType));
        });
    }
}
