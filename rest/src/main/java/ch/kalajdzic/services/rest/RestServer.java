/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.path.PartedPath;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.spi.HttpServerProvider;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestServer<T> {

    private static final Logger logger = Logger.getLogger(RestServer.class.getName());
    private final Map<HttpMethods, Container> containers;
    private final T service;
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final Object lock = new Object();
    private HttpServer server;

    public RestServer(T service) throws ReflectiveOperationException {
        containers = new AnnotationParser<>(service).parseService();
        this.service = service;
    }

    public void start(int port, int backlog) throws IOException {
        logger.info("Starting REST server on port " + port + " and backlog " + backlog);
        server = HttpServerProvider.provider().createHttpServer(new InetSocketAddress(port), backlog);
        server.createContext("/", exchange -> {
            logger.log(Level.FINEST, "Received request for " + exchange.getRequestURI());
            try (EasyHttpExchange httpExchange = new EasyHttpExchange(exchange)) {
                final Container container = containers.get(HttpMethods.valueOf(exchange.getRequestMethod()));
                final PartedPath requestPath = new PartedPath(exchange.getRequestURI().getPath());
                final AnnotatedMethod bestMethod = container.findBestMethod(requestPath);
                try {
                    ResponseConsumer requestProcessor = bestMethod.processRequest(requestPath);
                    synchronized (lock) {//TODO remove lock and make stuff thread-safe
                        requestProcessor.apply(httpExchange);
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "an exception was thrown while handling " + exchange.getRequestURI().getPath(), e);
                    httpExchange.setError(HttpURLConnection.HTTP_INTERNAL_ERROR);
                    httpExchange.clearResponseBody();
                }
            }
        });
        server.setExecutor(executor);
        server.start();
        logger.info("Started REST server");
    }

    public void stop(int delay) throws Exception {
        logger.info("Stopping REST server");
        if (server == null) {
            return;
        }
        server.stop(0);
        server = null;
        executor.shutdown();

        try {
            if (!executor.awaitTermination(delay, TimeUnit.SECONDS)) {
                executor.shutdownNow();
                if (!executor.awaitTermination(delay, TimeUnit.SECONDS)) {
                    logger.severe("Timed out waiting for server to shut down");
                }
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
        if (service instanceof AutoCloseable closeable) {
            closeable.close();
        }
        logger.info("Stopped REST server");
    }
}
