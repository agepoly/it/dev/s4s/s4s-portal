/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest.path;

public class Matching {

    public static Matching DONT_MATCH = new Matching(Integer.MIN_VALUE);
    public static Matching PERFECTLY_MATCH = new Matching(Integer.MAX_VALUE);
    private final int score;

    Matching(int score) {
        this.score = score;
    }

    public boolean dontMatch() {
        return this == DONT_MATCH;
    }

    public boolean perfectlyMatch() {
        return this == PERFECTLY_MATCH;
    }

    public int score() {
        return score;
    }
}
