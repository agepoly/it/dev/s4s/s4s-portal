/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.annotations.Delete;
import ch.kalajdzic.services.rest.annotations.Get;
import ch.kalajdzic.services.rest.annotations.Post;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public enum HttpMethods {

    GET(Get.class), POST(Post.class), DELETE(Delete.class);

    private final Class<? extends Annotation> annotation;

    HttpMethods(Class<? extends Annotation> annotation) {
        this.annotation = annotation;
    }

    public static Stream<HttpMethods> fromAnnotations(Method method) {
        return Arrays.stream(values())
                .filter(httpMethod -> httpMethod.isAnnotationPresent(method));
    }

    public static Optional<HttpMethods> fromString(String method) {
        try {
            return Optional.of(valueOf(method));
        } catch (IllegalArgumentException e) {
            return Optional.empty();
        }
    }

    public boolean isAnnotationPresent(Method method) {
        return method.isAnnotationPresent(annotation);
    }

}
