/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.annotations.ContentType;

import java.lang.reflect.Method;

public interface ContentTypes {
    String JSON = "application/json";
    String HTML = "text/html";
    String PLAIN_TEXT = "text/plain";
    String PNG = "image/png";
    String JAVASCRIPT = "application/javascript";
    String CSS = "text/css";

    static String from(Method method) {
        if (method.isAnnotationPresent(ContentType.class)) {
            return method.getAnnotation(ContentType.class).value();
        }
        return null;
    }
}
