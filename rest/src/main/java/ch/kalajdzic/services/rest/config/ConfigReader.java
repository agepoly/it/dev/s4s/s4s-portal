package ch.kalajdzic.services.rest.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.RecordComponent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ConfigReader<T extends Record> {

    private final Constructor<T> constructor;
    private final RecordComponent[] components;

    private ConfigReader(Class<T> record) {
        components = record.getRecordComponents();
        if (Arrays.stream(components).anyMatch(component -> !component.getType().equals(String.class))) {
            throw new RuntimeException("Record elements must be of type String");
        }
        Class<?>[] componentTypes = Arrays.stream(components).map(RecordComponent::getType).toArray(Class<?>[]::new);
        try {
            constructor = record.getDeclaredConstructor(componentTypes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends Record> T readConfig(Class<T> tClass, String filename) throws IOException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return new ConfigReader<>(tClass).parseFile(new File(filename));
    }

    private T parseFile(File file) throws IOException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Properties properties = new Properties();
        try (InputStream stream = new FileInputStream(file)) {
            properties.load(stream);
        }
        List<String> missing = new ArrayList<>();
        Object[] params = Arrays.stream(components).map(component -> {
            String value = properties.getProperty(component.getName());
            if (value == null) {
                missing.add(component.getName());
            }
            return value;
        }).toArray(String[]::new);
        if (!missing.isEmpty()) {
            throw new RuntimeException("Server configuration incomplete. Properties " + missing + " not found.");
        }
        return constructor.newInstance(params);

    }
}
