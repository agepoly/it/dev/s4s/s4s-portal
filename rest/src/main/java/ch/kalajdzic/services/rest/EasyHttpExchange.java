package ch.kalajdzic.services.rest;

import com.sun.net.httpserver.HttpExchange;

import java.io.Closeable;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Optional;

public class EasyHttpExchange implements Closeable {

    private final HttpExchange exchange;
    private byte[] body;
    private int errorCode = HttpURLConnection.HTTP_OK;

    public EasyHttpExchange(HttpExchange exchange) {
        this.exchange = exchange;
    }

    public Optional<String> getHeader(String name) {
        return Optional.ofNullable(exchange.getRequestHeaders().getFirst(name));
    }

    public void setError(int code) {
        errorCode = code;
    }

    public byte[] getRequestBody() throws IOException {
        return exchange.getRequestBody().readAllBytes();
    }

    public String getRequestBodyAsString() throws IOException {
        return new String(getRequestBody());
    }

    public void clearResponseBody() {
        body = null;
    }

    public void setResponseBody(String message) {
        this.body = message.getBytes();
    }

    public void setResponseBody(byte[] bytes) {
        this.body = bytes;
    }

    public void setUTF8() {
        exchange.getResponseHeaders().set("Content-Encoding", "UTF-8");
    }

    @Override
    public void close() throws IOException {
        try (exchange) {
            if (body == null) {
                body = ("HTTP ERROR CODE " + errorCode).getBytes();
            }
            exchange.sendResponseHeaders(errorCode, body.length);
            exchange.getResponseBody().write(body);
            exchange.getResponseBody().flush();
        }
    }

    public Optional<String> getResponseType() {
        return Optional.ofNullable(exchange.getResponseHeaders().getFirst("Content-Type"));
    }

    public void setResponseType(String contentType) {
        exchange.getResponseHeaders().set("Content-Type", contentType);
    }
}
