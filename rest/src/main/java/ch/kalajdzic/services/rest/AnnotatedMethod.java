/*
    Copyright (c) 2019 Johnny Kalajdzic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ch.kalajdzic.services.rest;

import ch.kalajdzic.services.rest.path.Matching;
import ch.kalajdzic.services.rest.path.MatchingScoreCalculator;
import ch.kalajdzic.services.rest.path.PartedPath;

import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class AnnotatedMethod {

    private final Object objectInstance;
    private final Method method;
    private final PartedPath path;
    private final String contentType;

    AnnotatedMethod(Object objectInstance, Method method, PartedPath path, String contentType) {
        this.objectInstance = objectInstance;
        this.method = method;
        this.path = path;
        this.contentType = contentType;
    }

    public Matching match(PartedPath requestPath) {
        return new MatchingScoreCalculator().calculate(path, requestPath);
    }

    private Object[] arguments(PartedPath requestPath) {
        final List<Object> result = new ArrayList<>();
        for (int i = 0; i < path.partCount(); i++) {
            if (path.isArgumentPart(i)) {
                result.add(URLDecoder.decode(requestPath.part(i), StandardCharsets.UTF_8));
            }
        }
        return result.toArray();
    }

    ResponseConsumer processRequest(PartedPath requestPath) {
        final Object[] arguments = arguments(requestPath);
        return e -> {
            e.setUTF8();
            if (contentType != null) {
                e.setResponseType(contentType);
            }
            ((ResponseConsumer) method.invoke(objectInstance, arguments)).apply(e);
        };
    }
}
