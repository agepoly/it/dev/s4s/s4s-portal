# Rest Service

This is a very simple REST service using only the jakarta servlet API and fasterxml jackson dependencies.

## License

Copyright (c) 2019 Johnny Kalajdzic

This program is free software: you can redistribute it and/or modify<br>
it under the terms of the GNU General Public License as published by<br>
the Free Software Foundation, either version 3 of the License, or<br>
(at your option) any later version.<br>

This program is distributed in the hope that it will be useful,<br>
but WITHOUT ANY WARRANTY; without even the implied warranty of<br>
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the<br>
GNU General Public License for more details.<br>

You should have received a copy of the GNU General Public License<br>
along with this program. If not, see <https://www.gnu.org/licenses/>.<br>

## Download

Download the jar rest-x.x.jar in the releases folder

## Add the JAR library with IntelliJ IDEA

1. Clone the project and install it with maven by running `mvn install`
2. Set up your project's pom.xml like this:
    ```
    <packaging>war</packaging>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.4.0</version>
            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
            <version>6.0.0</version>
        </dependency>
        <dependency>
            <groupId>ch.kalajdzic.services</groupId>
            <artifactId>rest</artifactId>
            <version>1.0</version>
        </dependency>
    </dependencies>
   ```
3. create the file `src/main/webapp/WEB-INF/web.xml` and fill it:
   ```
   <?xml version="1.0" encoding="UTF-8" ?>
   <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee"
            xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
            version="3.0">
       <servlet>
           <servlet-name>RestService</servlet-name>
           <servlet-class>ch.kalajdzic.services.rest.RestServer</servlet-class>
           <init-param>
               <param-name>server-class</param-name>
               <param-value></param-value>
           </init-param>
           <load-on-startup>0</load-on-startup>
       </servlet>
       <servlet-mapping>
           <servlet-name>RestService</servlet-name>
           <url-pattern>/api/*</url-pattern>
       </servlet-mapping>
   </web-app>
   ```
   The server-class value is the class of your Server. The server doesn't need anything special expect:
   - It can't be abstract
   - There should be a constructor with no arguments
   
   The Server class's constructor is called at server startup, and if it implements Closable, close() will be called when the server stops.
4. Enjoy !

## Setting up the web.xml


#### Here's another web.xml example

 ```
 <?xml version="1.0" encoding="UTF-8"?>
 <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee"
          xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
          version="3.0">
     <servlet-mapping>
         <servlet-name>default</servlet-name>
         <url-pattern>/css/*</url-pattern>
         <url-pattern>/javascript/*</url-pattern>
         <url-pattern>/lib/*</url-pattern>
         <url-pattern>/html/*</url-pattern>
         <url-pattern>/dev/*</url-pattern>
     </servlet-mapping>
     <servlet>
         <servlet-name>RestService</servlet-name>
         <servlet-class>ch.kalajdzic.services.rest.RestServer</servlet-class>
         <init-param>
             <param-name>server-class</param-name>
             <param-value>ch.kalajdzic.drproject.web.services.Server</param-value>
         </init-param>
         <load-on-startup>0</load-on-startup>
     </servlet>
     <servlet-mapping>
         <servlet-name>RestService</servlet-name>
         <url-pattern>/*</url-pattern>
     </servlet-mapping>
 </web-app>
 ```

## Documentation about services

If you want to have more services, add methods like this:<br>

```
@Child
@Path("/secondService")
public SecondService thisMethodLoadAnotherService() {
    return new SecondService();
}
```

Not that the @Path annotation is optional and any Service can load other ones with the @Child annotation.

A Service can have the @Path annotation on before the class's declaration, like this:

```
@Path("/titi")
public class TitiService {
```

All services are initialised when the RestService servlet is initialised by tomcat.

#### Using a service

If you want that your service «Toto» handle all /toto requests, you'll need to create a new Method in
the ```TotoService.class``` like the following:

```
@Get
public ResponseConsumer toto() {
return (response, request) -> {};
}
```

Not that the MainService can also handle requests like any other.
You can also use any other http method by specifying @Get, @Post, @Put, .. annotations

You can set the return type by filling the @ContentType annotation, like:

```
import static ch.kalajdzic.drproject.web.rest.ContentTypes.JSON;
[...]
@Post
@ContentType(JSON)
public ResonseConsumer simpleRequest() {
```

#### Parameters-in-path

With our service, you can retrieve parameters-in-path like this example:

```
@Get
@Path("sessions/$sessionId/report")
@ContentType(JSON)
public ResponseConsumer get(String sessionId) {
```

Note that the sessionId parameter will not contain any ```/``` character.
In fact any parameter-in-path will not contain that character.

#### Bonus

There's a cool class called `Configuration`, so you can load property files with ease.
