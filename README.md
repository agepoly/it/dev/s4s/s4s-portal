## The S4S server

This server is used by S4S and hosted on AGEP servers.

It has a lot of small parts and does many things.
Putting it in a single project makes it easier to change stuff around at the price of having to restart the whole thing
for small changes.

We could separate parts in different projects, but we'll end up with a lot of projects. And we can restart the server at
3am so downtime isn't that big of a deal anyway.

### How it works

The project is seperated into 4 different modules:

- `s4s-discord` manages the discord server of s4s athend enables us to automatically create channels and manage them. It
  also enables user to authenticate using a discord account
- `s4s-telegram` enables authentication using a telegram account
- `s4s-web` Is the web part of the server. (Both backend and frontend)
- `s4s-core` This is the main part of the project, it communicates with the web, telegram and discord components as well
  as the database
- `rest` A cool REST lib with similarities to Spring (in case we need Spring, it won't be too hard to switch)

### How to run for development

0. Have node installed
1. Uncomment the lines in `src/main/webapp/src/common/ServerAPI.js` to go to another port (or setup a local nginx serv). This allows hosting the UI and Serv on different ports, useful when debugging 
2. `cd s4s-web/src/main/webapp`
3. `pnpm dev` (maybe do `pnpm install` first)
4. Have java installed
5. Have a PostgreSQL server running
6. Copy `s4s-core/src/test/resources/s4s.properties` into `s4s.properties`
7. Fill the file with PostgreSQL credentials
8. Create a Discord bot and add it in a temp server. Fill the discord bot token
9. Create a Telegram bot. Fill the telegram bot token
10. Run s4s-web/src/main/java/ch/students4students/Main.java
11. Enjoy!

### How to run for production

0. compile with `./mvnw package`
1. Add a s4s.properties file at the working directory.
2. Run this jar: `s4s-web/target/s4s-web-jar-with-dependencies.jar` from your workdir
3. Enjoy!

### Project status

#### Implemented features:


- Telegram login works but username/id verification unimplemented.
- Discord login works but username/id verification unimplemented.
- RC-Selection staff page works, but unavailable

#### Features not done yet:

- RC-Selection doesn't save anything
- RC-Room page
- RC-Archive page
- Staff-Planning page
- Login with email

#### Possible Enhancements

- put the intbee webapp in this project
- move `rest` out of the project

### Library choices

- Why Java? Because most people at EPFL know this language, so a priority was given to this language. Also, it's a very
strong language specially for server for its robustness and performance.

- Why Vue.js? It's commonly used in other AGEP projects and much easier to learn compared to other frontend frameworks.

- What if we replaced ... with ...? Why not! As long as it's GPL compliant, you do what you want :D

### Authors & special thanks

- Johnny Kalajdzic (main dev, The IT manager of S4S in 2024)
- Mael Imhof (helped with SQL migration, the IT manager of S4S in 2023)
- Simon Lefort (helped with frontend, RC for the programmation course of S4S in 2024)
